const Mock = require('mockjs')

const data = Mock.mock({
  'items|32': [{
    'id|+1': 1,
    title: '@ctitle(2,4)房源接收',
    i: '发起人',
    'receive_total|1-200': 100,
    'receive_area|1-200': 100,
    // 'paper|1-20000': 1,
    'paper|1': [100, 2, 34,10],
    receive_time: '@datetime'
  }]
})

module.exports = [
  // 获取列表
  {
    url: '/tbAccept/selectAccept',
    type: 'get',
    response: config => {
      const items = data.items
      const { params, page, limit } = config.query
      console.log('mock', config.query, JSON.parse(params).paper);
      const p = JSON.parse(params)
      console.log('parse', p, p.length);
      let mockList = items

      if (p.paper || p.receive_total || p.receive_area) {
        console.log('if .....', p.paper);
        mockList = items.filter(i => {
          // console.log('mockList filter', i.paper, p.paper);

          if (i.receive_total == p.receive_total){
            console.log('receive_total false');
            return true
          }

          if (i.receive_area == p.receive_area){
            console.log('receive_area false');
            return true
          }

          if (i.paper == p.paper){
            console.log('paper false1');
            return true
          }
        })
      }

      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))

      return {
        code: 0,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  },
  // 创建or编辑
  {
    url: '/admin/createHospital',
    type: 'post',
    response: config => {
      let obj  = config.query
      if (obj.id) {
        data.items.some(u => {
          if (u.id === obj.id) {
            u.name = obj.name
            u.description = obj.description
            u.status = obj.status
            return true
          }
        })
        return {
          code: 20000,
          message: '修改成功'
        }
      }else{
        obj.id = Mock.Random.id()
        obj.created_at = Mock.mock('@now')
        data.items.unshift(obj)

        return {
          code: 20000,
          message: '添加成功'
        }
      }

    }
  },
  // 删除
  {
    url: '/tbAccept',
    type: 'delete',
    response: config => {
      console.log('config', config.body);
      let { id } = config.body
      console.log('id', id);
      if (!id) {
        return {
          code: -999,
          message: '参数不正确'
        }
      } else {
        data.items = data.items.filter(i => i.id !== id)
        return {
          code: 0,
          message: '删除成功'
        }
      }
    }
  },
  // 批量删除
  {
    url: '/admin/batchremove/hospital',
    type: 'post',
    response: config => {
      let str  = config.query.idStr
      let arr = str.split(',')
      data.items = data.items.filter(u => !arr.includes(u.id))
      return {
        code: 20000,
        message: '批量删除成功'
      }
    }
  }

]
