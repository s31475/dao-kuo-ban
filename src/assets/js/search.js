import XEUtils from 'xe-utils'
const searchEvent = {
    searchEvents (list, searchValue, PropsList) {
        let result = []
        const filterName = XEUtils.toValueString(searchValue).trim().toLowerCase()
        if (filterName) {
            const filterRE = new RegExp(filterName, 'gi')
            const rest = list.filter(item => PropsList.some(key => XEUtils.toValueString(item[key]).toLowerCase().indexOf(filterName) > -1))
            result = rest.map(row => {
                    const item = Object.assign({}, row)
                    PropsList.forEach(key => {
                        item[key] = XEUtils.toValueString(item[key]).replace(filterRE, match => `${match}`)
                    })
                    return item
            })
        } else {
            result = list
        }
        return result
    }
}
export default searchEvent