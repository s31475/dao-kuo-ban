import { login, getInfo } from '@/api/user'
import { getToken, setToken, removeToken, setRoleToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: null,
    introduction: '',
    roles: [],
    userObj: {}
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_USEROBJ: (state, user) => {
    state.userObj = user
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { username, password } = userInfo
    return new Promise((resolve, reject) => {
      login(userInfo).then(response => {
        console.log('store user login...', response);
        if (response.code == 500) {
          reject(response)
        }
        commit('SET_TOKEN', response.data);
        commit('SET_ROLES', response.roles);
        setToken(response.data)
        setRoleToken(response.roles)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    console.log('______________getInfo________________');
    return new Promise((resolve, reject) => {
      var token = getToken();
      if (token) {
        if (token) {
          try {
            getInfo().then(response => {
              const {data} = response

              if (data.code == 500) {
                reject('Verification failed, please Login again.')
              }

              const {roles, name, avatar, introduction, permissions} = data
              console.log('|||||||||||||', data, name, avatar, introduction);

              commit('SET_NAME', name)
              commit('SET_AVATAR', avatar)
              // commit('SET_INTRODUCTION', introduction)

              if (!roles || roles.length <= 0) {
                reject('getInfo: roles must be a non-null array!')
              }
              commit('SET_ROLES', roles)
              if (permissions && permissions.length > 0) {
                setPermissionsToken(permissions)

              }
              resolve(data)
            }).catch(error => {
              reject(error)
            })
          } catch (e) {

          }

        }
      }

    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      // logout(state.token).then(() => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      removeToken()
      resetRouter()
      commit('RESET_STATE')
      resolve()
      // }).catch(error => {
      //   reject(error)
      // })
    })
  },

  setRoles({ commit, state}, roles) {
    return new Promise((resolve, reject) => {
      commit('SET_ROLES', roles);
      resolve();
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

