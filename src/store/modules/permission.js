import { asyncRoutes, constantRoutes, notfound } from '@/router'
import { getResource, getResource3 } from "@/api/uc";
import { removeToken, getRoleToken, setRoleToken } from '@/utils/auth'
import { deepTraversal, deepTraversalLocal, transformTree, arr2TreeMenus } from "@/utils/tree";

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
// function hasPermission(roles, route) {
//   if (route.meta && route.meta.roles) {
//     return roles.some(role => route.meta.roles.includes(role))
//   } else {
//     return true
//   }
// }

// 后台返回所有的路由
function generateRoutes_ () {
  return new Promise(resolve => {
    var roleCode = getRoleToken();
    console.log('generateRoutes_', roleCode);
    getResource3(roleCode)
      .then(res => {
        console.log('getResource31 arr2TreeMenus', res.data);
        let tree = arr2TreeMenus(res.data);
        console.log('getResource32 arr2TreeMenus', tree);
        resolve(tree);
      })
      .catch(e => {
        console.log("err", e);
      });
  });
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, menus) {
  const res = []
  menus.forEach(item => {
    routes.forEach(route => {
      const tmp = { ...route }
      if (item.path === tmp.path) {
        tmp.meta.title = item.name
        if (item.children) {
          tmp.children = filterAsyncRoutes(tmp.children, item.children)
        }
        res.push(tmp)
      }
    })
  })
  res.concat(notfound)
  return res
}

const state = {
  routes: [],
  addRoutes: []
}

const mutations = {
  SET_ROUTES: (state, routes ) => {
    console.log('store permission mutations SET_ROUTES routes', routes);
    console.log('store permission mutations SET_ROUTES routes', asyncRoutes);
    console.log('store permission mutations SET_ROUTES constantRoutes', constantRoutes);

    // let sortConstantRoutes = [];
    // let sortDefault = 1; // 默认第一个
    // if (Number(sort) > 0) {
    //   sortDefault = sort;
    // }
    // // 对路由表进行过滤
    // sortConstantRoutes = constantRoutes.filter(item => {
    //   return item.sort === sortDefault;
    // });

    // let final = ComparisonAndDeal(resRouter, constantRoutes);
    console.log("p m SET_ROUTES routes", routes, routes.concat(asyncRoutes));
    state.routes = routes.concat(asyncRoutes);
  }
}

const actions = {
  generateRoutes({ commit }, roles) {
    console.log('generateRoutes123', roles);
    // setRoleToken(roles);
    // generateRoutes({ commit }, menus) {
    return new Promise(resolve => {
      generateRoutes_().then(res => {
        // let accessedRoutes;
        // if (roles.includes("admin")) {
        //   accessedRoutes = asyncRoutes || [];
        //   console.log('generateRoutes1', accessedRoutes);
        //   console.log('generateRoutes1', roles, sort);
        // } else {
        //   accessedRoutes = filterAsyncRoutes(asyncRoutes, roles);
        //   console.log('generateRoutes2', accessedRoutes);
        // }
        // console.log('.......ge)', res, asyncRoutes);
        commit('SET_ROUTES',
          res
        )
        resolve(res.concat(asyncRoutes));
      })
      // const accessedRoutes = filterAsyncRoutes(asyncRoutes, menus)
      // resolve(asyncRoutes);
    })
  }
}


// 设置隐藏
function setRouterMenu (obj, low_resRouter) {
  if (undefined == obj || null == obj || !obj instanceof Object) {
    return;
  }
  let item = low_resRouter.find(e => e.path === obj.path);
  if (item) {
    obj.id = item.id;
    obj.parentId = item.parentId;
    obj.menuId = item.menuId;
    obj.menuLevel = item.menuLevel;
    obj.hidden = false;
  } else {
    obj.hidden = true;
  }
  if (
    obj.children &&
    obj.children instanceof Array &&
    obj.children.length > 0
  ) {
    for (let child of obj.children) {
      setRouterMenu(child, low_resRouter);
    }
  }
}

// 处理菜单隐藏
function ComparisonAndDeal (resRouter, sortConstantRoutes) {
  if (!resRouter) return [];
  console.log("ComparisonAndDeal sortConstantRoutes1", sortConstantRoutes);
  let low_resRouter = deepTraversal(resRouter);
  // const low_constantRoutes = deepTraversalLocal(sortConstantRoutes); // local
  // low_constantRoutes.forEach(each => setRouterMenu(each, low_resRouter));
  console.log("ComparisonAndDeal low_resRouter", low_resRouter);
  console.log("ComparisonAndDeal resRouter", resRouter);
  sortConstantRoutes.forEach(each => setRouterMenu(each, low_resRouter));
  console.log("ComparisonAndDeal sortConstantRoutes2", sortConstantRoutes);
  // getTree(low_constantRoutes, "0");
  // console.log("menuData", transformTree(low_constantRoutes));
  // return transformTree(low_constantRoutes);
  return sortConstantRoutes;
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
