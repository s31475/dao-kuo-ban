import request from '@/utils/request2'
import {Login,userInfo} from './api'

export function login(data) {
  return request({
    url: Login,
    method: 'POST',
    data
  })
}

export function getInfo() {
  console.log('...........getInfo............');
  return request({
    url: userInfo,
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}

export function current() {
  return request({
    url: '/api-user/menus/current2',
    method: 'get'
  })
}
