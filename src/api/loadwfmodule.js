import request from '@/utils/request2'
import qs from 'qs'
import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN, SERVICE_FORM, SERVICE_BPM } from './api'

/**
 * 加载数据
 * @param data
 */
export function loadWfModule() {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/menu/menu/loadWfModule',
    method: 'POST'
  })
}

/**
 * 获取开始节点的按钮
 * @param typeKey
 */
export function getStartBtn(typeKey) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/start/btn/' + typeKey,
    method: 'GET'
  })
}
export function renderStartForm(defId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/render/start/get/renderForm/' + defId,
    method: 'GET'
  })
}

export function startDef(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/runtime/instance/v2/start',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function saveDraft(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/bpm/draft/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}
export function getDraft(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/bpm/draft/get',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 根据模型ID进行测试
 * @param data
 */
export function simulationBpm(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/models/v2/simulation/' + data.id,
    method: 'POST',
    data: JSON.stringify(data)
  })
}
export function simulationBpmVar(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/api/models/v2/simulation/required/' + id,
    method: 'POST'
  })
}
export function simulationBpmResult(businessKey) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM + '/a1bpmn/api/simulation/get/' + businessKey,
    method: 'GET'
  })
}
