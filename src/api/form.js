import request from '@/utils/request2'
import qs from 'qs'
import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN, SERVICE_FORM } from './api'

/**
 * 表单分类
 */
export function getTypesByKey() {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_ADMIN + '/a1bpmn/sys/sysType/v1/form/getTypesByKey',
    method: 'get'
  })
}
export function getBusinessTableListJson(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/bus/businessTable/listJson',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getFormModelTableListJson(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/design/models/listJson',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getFormModelTableListAllJson(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/design/models/listAll',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function formUpdateMain(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/design/models/updateMain?modelId=' + id,
    method: 'POST'
  })
}
export function formRemove(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/design/remove/' + id,
    method: 'GET'
  })
}
export function businessTableSave(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/businessTable/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}
export function businessObjectSave(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/businessObject/save',
    method: 'POST',
    data: JSON.stringify(data)
  })
}
export function businessObjectDel(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/businessObject/delete/' + id,
    method: 'GET'
  })
}
export function businessTableDel(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/businessTable/delete/' + id,
    method: 'GET'
  })
}
export function businessTableGet(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/businessTable/get/' + id,
    method: 'GET'
  })
}
export function businessObjectGet(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/businessObject/get/' + id,
    method: 'GET'
  })
}
export function businessTableCreate(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/businessTable/createTable/' + id,
    method: 'GET'
  })
}
export function businessTableColumnDelete(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/businessTable/column/delete/' + id,
    method: 'GET'
  })
}
export function businessTableColumnMetadataInfo(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/businessTable/metadataInfo',
    method: 'GET'
  })
}
export function businessTableColumnMetadataInfoByTableName(tableName) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/businessTable/metadataInfo/' + tableName,
    method: 'GET'
  })
}
export function businessObjectGetStruct(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/businessObject/getStruct/' + id,
    method: 'GET'
  })
}

export function businessObjectList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/bus/businessObject/listJson/',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function designSave(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/design/json/save',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 对话框查询
 * @param data
 */
export function customDialogList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/customDialog/listJson',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 保存对话框
 * @param data
 */
export function customDialogSave(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/customDialog/save',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function customDialogMetadataInfo(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/customDialog/metadataInfo/' + data,
    method: 'GET'
  })
}
export function businessTableMetadataInfo(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/bus/businessTable/metadataInfo/' + data,
    method: 'GET'
  })
}
export function customDialogGet(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/customDialog/get/' + data,
    method: 'GET'
  })
}
export function removeCustomDialog(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/customDialog/remove',
    method: 'POST',
    data: data
  })
}

export function updateFormModel(data) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/design/json/update/' + data.formKey,
    method: 'POST',
    data: data
  })
}
export function getFormModelVersion(formKey) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_FORM + '/form/design/version/get/' + formKey,
    method: 'POST'
  })
}

