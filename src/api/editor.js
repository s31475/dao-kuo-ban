import request from '@/utils/request'
import { get, put } from '@/utils/request'

export function imageUpload(data) {
  return request({
    url: '/api-user/upload/uploadFile',
    method: 'post',
    data: data
  })
}

export function releaseSubmit(data) {
  return request({
    url: '/api-user/tbNewsBulletin',
    method: 'post',
    data
  })
}

export function getReleaseContentList() {
  return request({
    url: '/api-user/tbPolicy',
    method: 'get'
  })
}

export function getReleaseContent(id) {
  return get('/api-user/tbPolicy', {
    id: id
  })
}

export function updateReleaseContent(info) {
  return put('/api-user/tbPolicy', {
    details: info['details'],
    id: info['id'],
    time: info['time'],
    name: info['name'],
    status: info['status'],
  })
}

