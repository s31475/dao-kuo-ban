import request from '@/utils/request'
import { deletefn, deletefn1, get, put, post } from '@/utils/request'
import qs from 'qs'
import {Login,userInfo,GET_RESOURCE3,SERVICE_ADMIN,SERVICE_FORM,SERVICE_BPM} from './api'

export function startDef(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/a1bpmn/api/runtime/instance/v2/start',
    method: 'POST',
    data: qs.stringify(data)
  })
}
