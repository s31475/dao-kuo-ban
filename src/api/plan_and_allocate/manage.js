import request from '@/utils/request'
import {SERVICE_LOONGTEK} from '../api'

// 调拨申请
export function getAllotStay(page, limit, allotStatus,engineeringProject, userName ) {
    return request({
      url: '/plan-allot456/tbAllotApply/selectAllotStay',
      method: 'post',
      data:{
        page, limit, allotStatus , engineeringProject, userName
      }
    })
}
// 根据 id 查调拨申请详情--区级计划详情
export function getAllotStayOne(allotId, id ) {
  return request({
    url: '/plan-allot456/tbAllotApply/selectAllotStayOne',
    method: 'get',
    params:{
      allotId, id
    }
  })
}
// 根据 id 查调拨申请详情--房源分配
export function getAllotStayTwo(allotId) {
  return request({
    url: '/plan-allot456/tbAllotApply/selectAllotStayTwo',
    method: 'get',
    params:{
      allotId
    }
  })
}
// 根据 id 查调拨申请详情--固定资产
export function getAllotStayThree(allotId) {
  return request({
    url: '/plan-allot456/tbAllotApply/selectAllotStayThree',
    method: 'get',
    params:{
      allotId
    }
  })
}
// 选择房子--修改房屋状态
export function putUpdateAllot(data) {
  return request({
    url: '/loongtek-member-cba/tbHousingBuild/updateAllot',
    method: 'post',
    data
  })
}
// 选择房子--调拨与房子关联表
export function putAllHousingt(data) {
  return request({
    url: '/plan-allot456/tbAllHousing',
    method: 'post',
    data
  })
}

// 查调拨申请绑定的房子
// export function putAllHousingt(data) {
//   return request({
//     url: '/plan-allot456/tbAllotApply/selectAllotStay',
//     method: 'post',
//     data
//   })
// }
