import request from '@/utils/request'
import axios from 'axios'
import qs from 'qs'
import { deletefn, deletefn1, get, put, post } from '@/utils/request'
import {Login,userInfo,GET_RESOURCE3,SERVICE_ADMIN,SERVICE_FORM,SERVICE_BPM,SERVICE_LOONGTEK} from './api'

export function addNodeOperate(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM+'/a1bpmn/api/cockpit/process-instance/addNode/' + data.taskId,
    method: 'POST',
    data: qs.stringify(data)
  })
}

export function getOutgoingFlows(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM+'/a1bpmn/api/cockpit/process-instance/outgoingFlows/' + id,
    method: 'get'
  })
}

export function getNextNode(taskId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url:SERVICE_BPM+ '/a1bpmn/api/cockpit/process-instance/getNextNode/' + taskId,
    method: 'get'
  })
}

export function completeTask(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM+'/a1bpmn/api/runtime/task/v2/complete',
    method: 'POST',
    data: qs.stringify(data)
  })
}

export function getTaskBtn(taskId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url:SERVICE_BPM+ '/a1bpmn/api/runtime/getTaskBtn/' + taskId,
    method: 'get'
  })
}

/**
 * 历史意见
 * @param id
 */
 export function opinionTask(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_BPM+'/a1bpmn/api/runtime/hisInstance/v1/nodeOpinion?instId=' + id,
    method: 'get'
  })
}

export function getTbCheckHousingByPG(data) {
  // console.log('getTbCheckHousing', data, page, limit);
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: `/pangu-admin/a1bpmn/api/runtime/task/v2/list`,
    method: 'post',
    data: qs.stringify(data)
  })
}

export function getTbCheckHousingByPGHistory(data) {
  // console.log('getTbCheckHousing', data, page, limit);
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: `/pangu-admin/a1bpmn/api/history/task/v2/list`,
    method: 'post',
    data: qs.stringify(data)
  })
}

export function renderByTaskId(id) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM + '/form/bus/render/get/' + id,
    method: 'get'
  })
}

export function renderHisForm(processInstanceId) {
  return request({
    headers: {
      'Content-Type': 'application/json'
    },
    url: SERVICE_FORM+'/form/bus/render/get/renderHisForm/' + processInstanceId,
    method: 'get'
  })
}

export function testPanguBpm(data) {
  return request({
      url: SERVICE_BPM + '/a1bpmn/api/runtime/instance/v2/start',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      data: qs.stringify(data)
  })
}

// 房源录入-------------------------------------------

export function getHouseInputInfo(params, page, limit, plotName, lotName) {
  // return get(SERVICE_LOONGTEK + '/tbPlotHousing/aaa', {
  //   params,
  //   page,
  //   limit, plotName, unit
  // })
  return get(SERVICE_LOONGTEK + '/tbPlotHousing/select', {
      params,
      page,
      limit, plotName, lotName
  })
}


export function getReceiveHouseInputInfo(params, page, limit, lotName, unit) {
  // return get(SERVICE_LOONGTEK + '/tbPlotHousing/aaa', {
  //   params,
  //   page,
  //   limit, plotName, unit
  // })
  return get(SERVICE_LOONGTEK + '/tbPlotHousing/select1', {
      params,
      page,
      limit, lotName, unit
  })
}


export function isSendingPutInto(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbPlotHousing/whetherToSend',
    method: 'put',
    data
  })
}

// 单条房屋录入信息
export function getHouseInputInfoById1(id, page, limit, receiveStatus) {
  console.log("id, page, limit, receiveStatus", id, page, limit, receiveStatus)
    return get(SERVICE_LOONGTEK + '/tbPlotHousing/selectPlot', {
      id,
      page,
      limit,
      receiveStatus
    })
}


export function addSingleHouse(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbPlotHousing/saveHousing',
    method: 'post',
    data
  })
}

export function deleteHouse(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbPlotHousing/removeHousing',
    method: 'post',
    data
  })
}

export function getAcceptDetails(id, soleId, page, limit, cardStatus) {
  console.log('______________getAcceptDetails', id, soleId);

  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_LOONGTEK + '/tbAccept/selectAcceptDetails',
    method: 'get',
    params: { id, soleId, page, limit, cardStatus }
  })
}

// 单条房屋录入信息
export function getHouseInputInfoById2(id, page, limit, receiveStatus, storageStatus, cardStatus) {
  console.log("id, page, limit, receiveStatus", id, page, limit, receiveStatus)
    return get(SERVICE_LOONGTEK + '/tbPlotHousing/selectHouseStorage', {
      id,
      page,
      limit,
      receiveStatus,
      storageStatus,
      cardStatus
    })
}
// 单条房屋录入信息
export function getHouseInputInfoById(id, dkName, floorNo, ladderNo, houseNo,allArea, page, limit, receiveStatus, cardStatus) {
  console.log("id, dkName, allArea, page, limit, receiveStatus", id, dkName, allArea, page, limit, receiveStatus)
    return get(SERVICE_LOONGTEK + '/tbPlotHousing/selectHousingBuild', {
      id,
      dkName,
      floorNo,
      ladderNo,
      houseNo,
      allArea,
      page,
      limit,
      receiveStatus,
      cardStatus,
      storageStatus: 0
    })
}
// 修改信息
export function putHouseInputInfo1(data) {
    return request({
        url: SERVICE_LOONGTEK + '/tbPlotHousing/updateEnt',
        method: 'put',
        data
    })
}
export function putHouseInputInfo2(data) {
    return request({
        url: SERVICE_LOONGTEK + '/tbAccept/update',
        method: 'put',
        data
    })
}
// 添加
export function addHouseInputInfo(data) {
    return request({
        url: SERVICE_LOONGTEK + '/tbPlotHousing/save',
        method: 'post',
        data
    })
}
// 删除
export function delHouseInputInfo(idList) {
    return deletefn1(SERVICE_LOONGTEK + '/tbPlotHousing/delete', {
        idList
    })
}

export function selectplot(page, limit) {
  return get(SERVICE_LOONGTEK + '/tbPlotHousing/select1', {
    page, limit
  })
}

export function downloadImage(file) {

  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_LOONGTEK + '/upload/down',
    method: 'get',
    responseType: 'blob',
    params: {
      file: file
    }
  })
}
const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // url = base url + request url
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 5000, // request timeout
})

export function uploadExcel(file) {
  console.log('uploadExcel', file);
  return service.post(SERVICE_LOONGTEK + '/tbPlotHousing/upload', { file: file }).then(res => {
    console.log('aaaa', res);
  })
  // return request({
  //   headers: {
  //     'Content-Type': 'multipart/form-data'
  //   },
  //   url: SERVICE_LOONGTEK + '/tbPlotHousing/upload',
  //   method: 'post',
  //   file
  // })
}

export function selectHouse(page, limit) {
  return get(SERVICE_LOONGTEK + '/tbPlotHousing/select2', {
    page, limit
  })
}
// 台账查册
// /tbTenement
/**
 * 自有物业台账
 */
export function getYears() {
  return get(SERVICE_LOONGTEK + '/tbTenement/year')
}

export function getTbTenement(enteringTime) {
  return get(SERVICE_LOONGTEK + '/tbTenement', {
    enteringTime
  })
}

// export function addTbTenement(data) {
//   return request({
//     url: SERVICE_LOONGTEK + '/tbTenement/add',
//     method: 'post',
//     data
//   })
// }

export function updateTbTenement(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbTenement',
    method: 'put',
    data
  })
}

export function deleteTbTenement(idList) {
  return delete(SERVICE_LOONGTEK + '/tbTenement', {
    idList
  })
}

export function selectTbTenement(params, page, limit, enteringTime, purpose) {
  return get(SERVICE_LOONGTEK + '/tbTenement/select', {
    params, page, limit, enteringTime, purpose
  })
}


/**
 * 小区物业台账
 */


 export function getTbRegion(enteringTime) {
  return get(SERVICE_LOONGTEK + '/tbRegion', {
    enteringTime
  })
}


export function addTbRegion(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbRegion/updateRegion',
    method: 'put',
    data
  })
}

export function selectTbRegion(plotName, houseType, totalArea, enteringTime, attribute) {
  return get(SERVICE_LOONGTEK + '/tbRegion/selectXq', {
    plotName, houseType, totalArea, enteringTime, attribute
  })
}

export function updateTbRegion(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbRegion',
    method: 'put',
    data
  })
}

/**
 * 查册台账接口
 */

 export function updateAudit(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbAudit',
    method: 'put',
    data
  })
}


export function addAudit(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbAudit',
    method: 'post',
    data
  })
}
// export function getHouseInputInfo() {
//     return request({
//       url: '/tbPlotHousing/select',
//       method: 'get'
//     })
// }

// export function delBuildingInfoById(id) {
//     return deletefn('/api-user/tbBuilding/delete/' + id)
// }

/**
 * 综合信息审批
 */



export function getPlotInfos(id, instId) {
  console.log('______________getAcceptDetails', id);

  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_LOONGTEK + '/TbPlotMsg/selectTbPlotMsg',
    method: 'get',
    params: {id, instId}
  })
}


export function getApprovalHouseInfos(idList, instId) {
  console.log('getApprovalHouseInfos', idList);

  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_LOONGTEK + '/tbHousingMsg/selectTbHousingMsg',
    method: 'get',
    params: { idList, instId }
  })
}

export function getTbCheckHousing(data, page, limit) {
  console.log('getTbCheckHousing', data, page, limit);
  return request({
    url: SERVICE_LOONGTEK + `/TbCheckHousing/page_check_housing/${page}/${limit}`,
    method: 'post',
    data
  })
}

export function testMock(title, page, limit) {
  return get('/vue-admin-template/table/list', { title, page, limit })
}

export function mockStandingBookOwn(year, page, limit) {
  return get('/mock/standingBook/standingBook_own', { year, page, limit})
}

export function mockStandingBookCommunity(year, page, limit) {
  return get('/mock/standingBook/standingBook_community', { year, page, limit})
}
export function mockHouseReceive(title, page, limit) {
  return get('/mock/table/house_receive', { title, page, limit })
}

/**
 * 房源建册
 */

// export function getTbHousingCopies (page, limit) {
//   // return request({
//   //   url: '/TbHousingCopies/selectTbHousingCopies',
//   //   method: 'post',
//   //   data
//   // })
//   return get('/TbHousingCopies', {
//     page, limit
//   })
// }

export function getTbHousingCopies(page, limit, data) {
  console.log('getTbHousingCopies1', data);
  return request({
    url: SERVICE_LOONGTEK + `/TbHousingCopies/page_houses/${page}/${limit}`,
    method: 'post',
    data
  })
}

export function getBuild (receiveStatus, page, limit, plotName) {
  return get(SERVICE_LOONGTEK + '/tbHousingBuild/selectBuild', {
    receiveStatus,page,limit,plotName
  })
}

export function getTbPlotPagePlots(page, limit, data) {
  console.log('getTbHousingCopies1', data);
  return request({
    url: SERVICE_LOONGTEK + '/tbHousingBuild/selectPlotRegulator',
    method: 'get',
    params: {
      checkStatus:2,
      page,
      limit,
      ...data
    }
  })
}

export function saveEditingTbPlotMsg(data) {
  return request({
    url: SERVICE_LOONGTEK + '/TbPlotMsg/saveTbPlotMsg',
    method: 'post',
    data
  })
}


export function saveEditingTbHouseMsg(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbHousingMsg/saveTbHousingMsg',
    method: 'post',
    data
  })
}

export function savePutIntoStorage(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbHousingBuild/storageApprove',
    method: 'post',
    data
  })
}
export function sendToApprove(soleId, checkStatus) {
  return request({
    url: SERVICE_LOONGTEK + '/tbAccept/updateCheckStatus',
    method: 'post',
    data: {
      soleId: soleId,
      checkStatus: checkStatus
    }
  })
}

export function editPlotApprove(data) {
  return request({
    url: SERVICE_LOONGTEK + '/TbPlotMsg/updateTbPlotMsg',
    method: 'put',
    data
  })
}

export function editHouseApprove(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbHousingMsg/updateTbHousingMsg',
    method: 'put',
    data
  })
}
/**
 * 房源接收
 */

 export function saveAccept(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbAccept/saveAccept',
    method: 'post',
    data
  })
}
 export function getHouseType() {
  return get(SERVICE_LOONGTEK + '/tbPlotHousing/selectHouseType')
}
export function getAccept (page, limit, account, receiveArea, receiveCycle) {
  console.log('getAccept', page, limit, account, receiveArea, receiveCycle)


  return request({
    url: SERVICE_LOONGTEK + '/tbAccept/selectAccept',
    method: 'get',
    params: {page, limit, account, receiveArea, receiveCycle}
  })
}


export function deleteAccept(id) {
  console.log('deleteAccept111', id)
  // return delete('/tbAccept', {
  //   id
  // })
  return request({
    url: SERVICE_LOONGTEK + '/tbAccept/delete',
    method: 'get',
    params: {soleId: id}
  })
}


/**
 * 房源入库
 * @returns
 *
 */

export function deleteTbCheckHousing(id) {
  return request({
    url: SERVICE_LOONGTEK + `/TbCheckHousing/delete/${id}`,
    method: 'delete'
  })
}
export function getTbCheckHousingStoredHouse(data, page, limit) {
  console.log('getTbHousingCopies1', data);
  return request({
    url: SERVICE_LOONGTEK + `/tbHousingBuild/selectBuildList`,
    method: 'post',
    data
  })
}

export function getStoredHouseById(idList) {
  console.log('getStoredHouseById', idList);
  return request({
    url: SERVICE_LOONGTEK + `/tbHousingMsgBuild/selectBuildHouse`,
    method: 'get',
    params: {idList}
  })
}
export function getPutIntoStorageDetail(soleId, page, limit) {
  return request({
    url: SERVICE_LOONGTEK + '/tbHousingBuild/selectStorageDetails',
    method: 'get',
    params: {
      soleId,
      page,
      limit
    }
  })
}

export function getStorageHouseDetail(idList, instId) {
  console.log('getStorageHouseDetail', idList);
  return request({
    url: SERVICE_LOONGTEK + '/tbHousingMsgBuild/selectTbHousingMsgBuild',
    method: 'get',
    params: { idList, instId }
  })
}

export function putIntoStorageApprove(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbHousingBuild/updateCheckStatus',
    method: 'post',
    data
  })
}

export function editStorageHouseApprove(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbHousingMsgBuild/saveTbHousingMsgBuild',
    method: 'post',
    data
  })
}


export function editStorageHouseApprove1(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbHousingMsgBuild/updateTbHousingMsgBuild',
    method: 'put',
    data
  })
}
// 小区信息管理

export function plotMsgList () {
  return get('/', {

  })
}

// export function addAudit(data) {
//   return request({
//     url: '/tbAudit',
//     method: 'post',
//     data
//   })
// }


// export function getAcceptMock (params, page, limit) {
//   return get('/mock/tbAccept/selectAccept', {
//     params, page, limit
//   })
// }

// 机外台账
/**
 *
 */

export function getTbOutdoorAccount (data, page, limit) {
  return request({
    url: SERVICE_LOONGTEK + `/tb_outdoor_account/page_outdoor_account/${page}/${limit}`,
    method: 'post',
    data
  })
}

// 经营性租赁台账

export function getTbLeaseAccount (page, limit, data) {
  return request({
    url: SERVICE_LOONGTEK + `/tbLeaseAccount/page_lease_account/${page}/${limit}`,
    method: 'post',
    data
  })
}


export function getDataHave (data) {
  return request({
    url: SERVICE_LOONGTEK + `/tbDataStatistics/selectDataHave`,
    method: 'post',
    data
  })
}
export function getDataHaveNone (data) {
  return request({
    url: SERVICE_LOONGTEK + `/tbDataStatistics/selectDataNone`,
    method: 'post',
    data
  })
}


// feng 20220302--------------start-------
// export function addTbTenement(data) {
//   return request({
//     url: SERVICE_LOONGTEK + '/tbTenement/add',
//     method: 'post',
//     data
//   })
// }
export function addTbTenement(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbTenement/updateTbTenement',
    method: 'put',
    data
  })
}
export function selectTbTenement1(params) {
  return get(SERVICE_LOONGTEK + '/tbTenement/select', params)
}

// --------------------拆迁台账
export function getRegionAll(params) {
  return request({
    url: SERVICE_LOONGTEK + '/tbRemove/selectAll',
    method: 'get',
    params
  })
}
export function getRegionOne(params) {
  return request({
    url: SERVICE_LOONGTEK + '/tbRemove/selectXq',
    method: 'get',
    params
  })
}
export function addRegionList(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbRemove/updateAudit',
    method: 'put',
    data
  })
}
// 各地拆迁台账导入
export function uploadRegionCsv(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbRemove/upload',
    method: 'post',
    data
  })
}
// feng 20220302--------------end-------

// feng 20220302-----------start---------
export function getAuditAll(params) {
  return request({
    url: SERVICE_LOONGTEK + '/tbAudit/selectAll',
    method: 'get',
    params
  })
}
export function addAudit1(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbAudit/updateAudit',
    method: 'put',
    data
  })
}
export function getAuditOne(params) {
  return request({
    url: SERVICE_LOONGTEK + '/tbAudit/selectXq',
    method: 'get',
    params
  })
}
// 自有物业台账导入
export function uploadOwnCsv(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbTenement/upload',
    method: 'post',
    data
  })
}
// 查册台账导入
export function uploadSearchCsv(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbAudit/upload',
    method: 'post',
    data
  })
}
// 小区台账导入
export function uploadCommunityCsv(data) {
  return request({
    url: SERVICE_LOONGTEK + '/tbRegion/upload',
    method: 'post',
    data
  })
}
// feng 20220302-----------end---------


export function getStatisticsMenu(attribute) {
  return request({
    url: SERVICE_LOONGTEK + '/tbDataStatistics/selectStatistics',
    method: 'get',
    params: { attribute }
  })
}


export function getPutIntoStatistics(storageTime, attribute) {
  return request({
    url: SERVICE_LOONGTEK + '/tbDataStatistics/storageMapsDistrict',
    method: 'get',
    params: { attribute, storageTime }
  })
}


export function getIntoStatistics(storageTime, attribute) {
  return request({
    url: SERVICE_LOONGTEK + '/tbDataStatistics/cardMapsDistrict',
    method: 'get',
    params: { attribute, storageTime }
  })
}


export function getPaperStatistics(storageTime, attribute) {
  return request({
    url: SERVICE_LOONGTEK + '/tbDataStatistics/paperMapsDistrict',
    method: 'get',
    params: { attribute, storageTime }
  })
}

export function getHouseTypeStatistics(storageTime, attribute) {
  return request({
    url: SERVICE_LOONGTEK + '/tbDataStatistics/houseMapsDistrict',
    method: 'get',
    params: { attribute, storageTime }
  })
}

// 市
export function getPutIntoStatistics1(storageTime, attribute) {
  return request({
    url: SERVICE_LOONGTEK + '/tbDataStatistics/storageMaps',
    method: 'get',
    params: { storageTime }
  })
}


export function getIntoStatistics1(storageTime, attribute) {
  return request({
    url: SERVICE_LOONGTEK + '/tbDataStatistics/cardMaps',
    method: 'get',
    params: { storageTime }
  })
}


export function getPaperStatistics1(storageTime, attribute) {
  return request({
    url: SERVICE_LOONGTEK + '/tbDataStatistics/paperMaps',
    method: 'get',
    params: { storageTime }
  })
}

export function getHouseTypeStatistics1(storageTime, attribute) {
  return request({
    url: SERVICE_LOONGTEK + '/tbDataStatistics/houseMaps',
    method: 'get',
    params: { storageTime }
  })
}