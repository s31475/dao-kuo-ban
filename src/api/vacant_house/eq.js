import request from '@/utils/request'

/**
 * 新增设备
 * @param {*} data
 * @returns
 */
export function addEQ(data) {
  return request({
    url: '/user-kzf12/sysEquip',
    method: 'post',
    data
  })
}
/**
 * 修改设备
 * @param {*} data
 * @returns
 */
export function editEQ(data) {
  return request({
    url: '/user-kzf12/sysEquip',
    method: 'put',
    data
  })
}
/**
 * 删除设备
 * @param {*} params
 * @returns
 */
export function delEQ(params) {
  return request({
    url: '/user-kzf12/sysEquip',
    method: 'delete',
    params
  })
}
/**
 * 获取区域信息
 * @returns
 */
export function getListSysArea(params) {
  return request({
    url: '/user-kzf12/sysArea',
    method: 'get',
    params
  })
}

/**
 *
 * @returns 获取小区信息
 */
export function getListSysVillage(params) {
  return request({
    url: '/user-kzf12/sysVillage',
    method: 'get',
    params
  })
}
/**
 * 设备类型
 * @param {*} params
 * @returns
 */
export function getListsysNodeType(params) {
  return request({
    url: '/user-kzf12/sysNodeType',
    method: 'get',
    params
  })
}
/**
 * 获取设备网关数据和模糊查询
 * @param {*} params
 * @returns
 */
// export function getListOnlyWG(params) {
//   return request({
//     url: '/kzf',
//     method: 'get',
//     params
//   })
// }

/**
 * 更改网关操作
 * @param {*} params
 * @returns
 */
// export function updateByHouseId(params) {
//   return request({
//     url: '/sysEquip/updateByHouseId',
//     method: 'get',
//     params
//   })
// }
/**
 * 房子绑定网关
 * @param {*} params
 * @returns
 */
export function updateByHouseId(data) {
  return request({
    url: '/user-kzf12/kzf/updateHouse',
    method: 'put',
    data
  })
}

// 首页 广州市巡检记录
export function getAreaMonth() {
  return request({
    url: '/user-kzf12/sysHouse/areaMonth',
    method: 'get',
  })
}
// 首页  区属按周统计巡查数据
export function getAreaHouseIdList(params) {
  return request({
    url: '/user-kzf12/sysHouse/areaHouseIdList',
    method: 'get',
    params
  })
}
// 首页  区属按周统计告警数量
export function getAreaWarnHouseIdList(params) {
  return request({
    url: '/user-kzf12/sysHouse/areaWarnHouseIdList',
    method: 'get',
    params
  })
}

// 首页 小区 按周统计巡检次数
export function getVillageWeekCount(params) {
  return request({
    url: '/user-kzf12/sysHouse/villageWeekCount',
    method: 'get',
    params
  })
}

// 首页 小区 按周统计告警数量
export function getVillageWarnWeekCount(params) {
  return request({
    url: '/user-kzf12/sysHouse/villageWarnWeekCount',
    method: 'get',
    params
  })
}

// 首页 小区实时告警
export function getPlotWarn(params) {
  return request({
    url: '/user-kzf12/sysVillage/plotWarn',
    method: 'get',
    params
  })
}
