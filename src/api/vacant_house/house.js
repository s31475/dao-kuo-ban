import request from '@/utils/request'


/**
 * 房屋与网关解绑
 * @param {*} data
 * @returns
 */
 export function updateRemoveHouse(data) {
  return request({
    url: '/user-kzf12/kzf/updateRemoveHouse',
    method: 'put',
    data
  })
}
/**
 *
 * @returns 获取房屋信息
 */
export function getList(params) {
  return request({
    url: '/user-kzf12/sysHouse/selectHouseList',
    method: 'get',
    params
  })
}

/**
 * 新增房屋信息
 * @param {*} data
 * @returns
 */
export function addHouse(data) {
  return request({
    url: '/user-kzf12/sysHouse',
    method: 'post',
    data
  })
}

/**
 * 修改房屋信息
 * @param {*} data
 * @returns
 */
export function editHouse(data) {
  return request({
    url: '/user-kzf12/sysHouse',
    method: 'put',
    data
  })
}
/**
 * 删除房屋信息
 * @param {*} params
 * @returns
 */
export function delHouse(params) {
  return request({
    url: '/user-kzf12/sysHouse',
    method: 'delete',
    params
  })
}

/**
 * 模糊获取小区信息
 * @param {*} params
 * @returns
 */
export function getListSysVillage(params) {
  return request({
    url: '/user-kzf12/sysVillage/findByName',
    method: 'get',
    params
  })
}
/**
 * 获取小区信息
 * @param {*} data
 * @returns
 */
export function getListSysVillageByAreaId(params) {
  return request({
    url: '/user-kzf12/sysVillage',
    method: 'get',
    params
  })
}
/**
 * 添加小区信息
 * @param {*} data
 * @returns
 */
export function addSysVillage(data) {
  return request({
    url: '/user-kzf12/sysVillage',
    method: 'post',
    data
  })
}

export function getListSysVillageById(id) {
  return request({
    url: `/sysVillage/${id}`,
    method: 'get'
  })
}

/**
 * 获取市级信息数量
 * @param {*} id
 * @returns
 */
export function getNum(id) {
  return request({
    url: '/user-kzf12/sysArea/getNum',
    method: 'get',
    params: {
      areaId: id
    }
  })
}

/**
 * 获取小区面积,房屋数量
 * @param {*} id
 * @returns
 */
export function getByVillageId(id) {
  return request({
    url: '/user-kzf12/sysVillage/getByVillageId',
    method: 'get',
    params: {
      villageId: id
    }
  })
}

/**
 * 获取区报警信息
 * @returns
 */
export function getWarnNum() {
  return request({
    url: '/user-kzf12/sysHouse/selectCountWarn',
    method: 'get'
  })
}

/**
 * 获取各区房屋数量
 * @returns
 */
export function getHouseNum() {
  return request({
    url: '/user-kzf12/sysHouse/selectCountHouse',
    method: 'get'
  })
}

/**
 * 获取首页告警表数据
 * @param {*} status
 * @returns
 */
export function getWarnHistory(params) {
  return request({
    url: '/user-kzf12/sysHouse/selectHouseList',
    method: 'get',
    params: params
  })
}

/**
 * 更新处理状态--根据房屋id处理房屋下所有告警记录状态，为已处理
 * @param {*} params
 * @returns
 */
export function updateDealStatus(data) {
  return request({
    url: '/user-kzf12/sysHouse/updateAllWarn',
    method: 'put',
    data
  })
}
/**
 * 更新处理状态--弹窗的处理告警信息
 * @param {*} params
 * @returns
 */
 export function updateOneWarn(data) {
  return request({
    url: '/user-kzf12/sysHouse/updateOneWarn',
    method: 'put',
    data
  })
}

