import request from '@/utils/request'

/**
 * 获取网关管理
 * @param {*} params
 * @returns
 */
// nodeType 1 网关
 export function getGatewayInfoList(data) {
  return request({
    url: '/user-kzf12/kzf/selectGateway',
    method: 'get',
    params:data
  })
}
// 绑定设备--根据id查网关
  export function getGatewayInfoListById(id, equipId) {
    return request({
      url: '/user-kzf12/kzf/selectParentId',
      method: 'get',
      params:{
        id,equipId
      }
    })
  }
  /**
 * 添加网关
 * @param {*} params
 * @returns
 */
 export function addGatewayInfo(data) {
    return request({
      url: '/user-kzf12/sysGateway/saveGateway',
      method: 'post',
      data
    })
  }
/**
 * 删除网关
 * @param {*} params
 * @returns
 */
 export function delGatewayInfoById(idList) {
    return request({
      url: '/user-kzf12/kzf',
      method: 'delete',
      params:{
        idList
      }
    })
}

/**
 * 修改网关
 * @param {*} params
 * @returns
 */
 export function editGatewayInfoById(data) {
    return request({
      url: '/user-kzf12/kzf/updateGateway',
      method: 'put',
      data
    })
}
/**
 * 查网关可以绑定的设备信息
 * @param {*} params
 * @returns
 */
 export function getEQInfoByNoUse(data) {
  return request({
    url: '/user-kzf12/kzf/selectGatewayBuild',
    method: 'get',
    params: data
  })
}
/**
 * 设备信息绑定网关
 * @param {*} params
 * @returns
 */
 export function binGatewayByEQ(data) {
  return request({
    url: '/user-kzf12/kzf',
    method: 'put',
    data
  })
}
/**
 * 设备信息绑定网关---解绑
 * @param {*} params
 * @returns
 */
 export function delBinGatewayByEQ(data) {
  return request({
    url: '/user-kzf12/kzf',
    method: 'put',
    data
  })
}
/**
 * 查网关厂家
 * @param {*} params
 * @returns
 */
 export function getGatewayFactory(nodeId) {
  return request({
    url: '/user-kzf12/sysFactory',
    method: 'get',
    params:{
      nodeId
    }
  })
}