import request from '@/utils/request'

/** 上传 */
export function uploadFile(data) {
  return request({
    url: '/user-kzf12/upload/uploadFile',
    method: 'post',
    data
  })
}

// 删除图片
export function delFile(url) {
  return request({
    url: '/user-kzf12/upload/delFile',
    method: 'get',
    params: {
      url: url
    }
  })
}
