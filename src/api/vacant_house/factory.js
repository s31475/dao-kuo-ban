
import request from '@/utils/request'


/**
 * 厂家信息查询
 * @param {*} params
 * @returns
 */
 export function getFactoryList(params) {
  return request({
    url: '/user-kzf12/sysFactory',
    method: 'get',
    params
  })
}
/**
 * 添加厂家信息
 * @param {*} data
 * @returns
 */
 export function addFactory(data) {
    return request({
      url: '/user-kzf12/sysFactory',
      method: 'post',
      data
    })
}
/**
 * 删除厂家信息
 * @param {*} params
 * @returns
 */
 export function delFactory(idList) {
    return request({
      url: '/user-kzf12/sysFactory',
      method: 'delete',
      params: {
          idList
      }
    })
}
/**
 * 修改厂家信息
 * @param {*} data
 * @returns
 */
 export function putFactory(data) {
    return request({
      url: '/user-kzf12/sysFactory',
      method: 'put',
      data
    })
}

/**
 * 添加设备信息
 * @param {*} data
 * @returns
 */
 export function addEq(data) {
    return request({
      url: '/user-kzf12/sysNodeType',
      method: 'post',
      data
    })
}
/**
 * 修改设备信息
 * @param {*} data
 * @returns
 */
 export function putEq(data) {
    return request({
      url: '/user-kzf12/sysNodeType',
      method: 'put',
      data
    })
}
/**
 * 删除设备信息
 * @param {*} params
 * @returns
 */
 export function delEq(idList) {
    return request({
      url: '/user-kzf12/sysNodeType',
      method: 'delete',
      params: {
            idList
        }
    })
}