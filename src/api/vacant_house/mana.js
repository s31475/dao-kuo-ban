import request from '@/utils/request'

/**
 * 获取设备历史告警信息
 * @param {*} params
 * @returns
 */
export function getHistoryList(data) {
  return request({
    url: '/user-kzf12/sysHouse/selectWarn',
    method: 'get',
    params: data
  })
}

export function getAccessToken() {
  return request({
    url: '/user-kzf12/monitor/getAccessToken',
    method: 'post',
    data: {}
  })
}

// 获取播放的token
export function getKitToken(token, deviceId) {
  return request({
    url: '/user-kzf12/monitor/getKitToken',
    method: 'post',
    data: {
      token, deviceId
    }
  })
}

// monitor/getAll

// 查房屋详情
export function getHouseDetails(id) {
  return request({
    url: '/user-kzf12/sysHouse/selectHouseDetails',
    method: 'get',
    params: {
      id
    }
  })
}
