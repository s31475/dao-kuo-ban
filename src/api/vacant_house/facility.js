import request from '@/utils/request'

/**
 * 查设备信息
 * @param {*} params
 * @returns
 */
 export function getEQList(data) {
  return request({
    url: '/user-kzf12/kzf/selectGateway',
    method: 'get',
    params:data
  })
}

/**
 * 获取设备类型
 * @param {*} params
 * @returns
 */
 export function getEqTypeList() {
    return request({
      url: '/user-kzf12/sysNodeType',
      method: 'get'
    })
  }
/**
 * 添加摄像头
 * @param {*} params
 * @returns
 */
 export function addCamera(data) {
  return request({
    url: '/user-kzf12/sysCamera/saveCamera',
    method: 'post',
    data
  })
}
/**
 * 添加刷卡器
 * @param {*} params
 * @returns
 */
 export function addImprinter(data) {
  return request({
    url: '/user-kzf12/sysImprinter/saveImprinter',
    method: 'post',
    data
  })
}
/**
 * 添加烟感
 * @param {*} params
 * @returns
 */
 export function addSmoking(data) {
  return request({
    url: '/user-kzf12/sysSmoke/saveSmoke',
    method: 'post',
    data
  })
}
/**
 * 添加红外
 * @param {*} params
 * @returns
 */
 export function addInfrared(data) {
  return request({
    url: '/user-kzf12/sysInfrared/saveInfrared',
    method: 'post',
    data
  })
}
/**
 * 修改摄像头
 * @param {*} params
 * @returns
 */
 export function putCamera(data) {
  return request({
    url: '/user-kzf12/sysCamera',
    method: 'put',
    data
  })
}
/**
 * 修改刷卡器
 * @param {*} params
 * @returns
 */
 export function putImprinter(data) {
  return request({
    url: '/user-kzf12/sysImprinter',
    method: 'put',
    data
  })
}
/**
 * 修改烟感
 * @param {*} params
 * @returns
 */
 export function putSmoking(data) {
  return request({
    url: '/user-kzf12/sysSmoke',
    method: 'put',
    data
  })
}
/**
 * 修改红外
 * @param {*} params
 * @returns
 */
 export function putInfrared(data) {
  return request({
    url: '/user-kzf12/sysInfrared',
    method: 'put',
    data
  })
}
/**
 * 删除摄像头
 * @param {*} params
 * @returns
 */
 export function delCamera(idList) {
  return request({
    url: '/user-kzf12/sysCamera',
    method: 'delete',
    params:{
      idList
    }
  })
}
/**
 * 删除刷卡器
 * @param {*} params
 * @returns
 */
 export function delImprinter(idList) {
  return request({
    url: '/user-kzf12/sysImprinter',
    method: 'delete',
    params:{
      idList
    }
  })
}
/**
 * 删除烟感
 * @param {*} params
 * @returns
 */
 export function delSmoking(idList) {
  return request({
    url: '/user-kzf12/sysSmoke',
    method: 'delete',
    params:{
      idList
    }
  })
}
/**
 * 删除红外
 * @param {*} params
 * @returns
 */
 export function delInfrared(idList) {
  return request({
    url: '/user-kzf12/sysInfrared',
    method: 'delete',
    params:{
      idList
    }
  })
}
/**
 * 导入设备
 * @param {*} params
 * @returns
 */
 export function importFacility(data) {
  return request({
    url: '/user-kzf12/sysCamera/upload',
    method: 'post',
    data
  })
}
