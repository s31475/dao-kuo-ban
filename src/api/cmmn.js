import request from '@/utils/request2'
import qs from 'qs'
import {Login,userInfo,GET_RESOURCE3,SERVICE_ADMIN,SERVICE_FORM,SERVICE_BPM} from './api'

/**
 * 获取CMMN模型的分类
 */
export function getCMMNModelCategory() {
  return request({

    url: SERVICE_ADMIN+'/a1bpmn/sys/sysType/v1/cmmn/getTypesByKey',
    method: 'get'

  })
}

/**
 * 保存模型
 * @param data
 */
export function saveDmn(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/dmn/design/save',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function startCmmn(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/start/'+id,
    method: 'POST',
  })
}

/**
 * 获取所有版本的模型
 * @param data
 */
export function getAllVersionModel(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/list/version/task',
    method: 'POST',
    data: qs.stringify(data)
  })
}



/**
 * 获取模型
 * @param data
 */
export function getModelerJson(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/list/all',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * 查询版本明细
 * @param data
 */
export function getVersion(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/dmn/design/page/list/all',
    method: 'POST',
    data: qs.stringify(data)
  })
}
/**
 * 设置主要版本
 * @param data
 */
export function setMain(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/main/'+id,
    method: 'GET',
  })
}



/**
 * 发布模型
 * @param id
 */
export function publishModel(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/publish/'+id,
    method: 'GET',
  })
}


/**
 * 获取运行计划项管理
 * @param data
 */
export function getInstanceList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/instance/getInstanceList/all',
    method: 'POST',
    data: qs.stringify(data)
  })
}

/**
 * start 获取运行计划项管理
 * @param data
 */
export function getPlanitemList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/planitem/get/all',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function disablePlanitem(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/planitem/disable/instance/'+id,
    method: 'GET',
  })
}
export function enablePlanitem(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM+ '/api/cmmn/planitem/enable/instance/'+id,
    method: 'GET',
  })
}
export function triggerPlanitem(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/planitem/trigger/instance/'+id,
    method: 'GET',
  })
}
export function terminatePlanitem(id) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/planitem/terminate/instance/'+id,
    method: 'GET',
  })
}
/**
 * end 获取运行计划项管理
 */


export function getMilestoneList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/milestone/page/all',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getDeployment(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/deployment/page/all',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getCache(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/monitor/cache',
    method: 'GET',
    data: qs.stringify(data)
  })
}
export function getHisInstanceList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/instance/history/getInstanceList/all',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getHisPlanItem(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/planitem/history/get/all',
    method: 'POST',
    data: qs.stringify(data)
  })
}
export function getHisMilestoneList(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url:  SERVICE_BPM+'/api/cmmn/milestone/his/page/all',
    method: 'POST',
    data: qs.stringify(data)
  })
}





