import request from '@/utils/request2'
import qs from 'qs'

import { Login, userInfo, GET_RESOURCE3, SERVICE_ADMIN, SERVICE_FORM, SERVICE_BPM } from './api'

/**
 * 获取任务数量
 * @param data
 */
export function getTaskCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/api/task/count/get',
    method: 'post',
    data: qs.stringify(data)
  })
}
export function getStartCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/api/start/count/get',
    method: 'post',
    data: qs.stringify(data)
  })
}

/**
 * 获取消息的数量
 * @param data
 */
export function getJmsCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/api/msg/list',
    method: 'post',
    data: qs.stringify(data)
  })
}

/**
 * 获取通知的数量
 * @param data
 */
export function getMsgCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/api/commu/receiver/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取表单模型数量
 * @param data
 */
export function getFormModelCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/form/api/model/count/get',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取表单模型分类数量
 * @param data
 */
export function getFormModelGroupCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/form/api/model/group/count/get',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取表单定义数量
 * @param data
 */
export function getFormDefCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/form/api/def/count/get',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取表单定义分类数量
 * @param data
 */
export function getFormDefGroupCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/form/api/def/group/count/get',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取业务对象分类数量
 * @param data
 */
export function getFormBusGroupCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/form/api/bus/group/count/get',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 在途表单实例相关数量相关查询
 * @param data
 */
export function getFormInstanceCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/form/api/form/instance/count/get',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取表单对象数量
 * @param data
 */
export function getFormBusCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/form/api/bus/count/get',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取在途任务+历史完成任务统计
 * @param data
 */
export function getProcessTaskCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/process/api/task/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 在途活动+历史完成活动统计
 * @param data
 */
export function getProcessActCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/process/api/act/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 模型统计
 * @param data
 */
export function getModelCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/process/api/model/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 模型分组统计
 * @param data
 */
export function getModelGroupCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/process/api/model/group/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 运行任务和历史任务分类统计
 * @param data
 */
export function getTaskGroupCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/process/api/task/group/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 发起实例统计
 * @param data
 */
export function getInstanceCount(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/process/api/instance/list',
    method: 'post',
    data: qs.stringify(data)
  })
}

/**
 * 3M模型统计
 * @param data
 */
export function getModelReport(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/model/api/model/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 3M模型实例统计
 * @param data
 */
export function getModelInstanceReport(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/model/api/instance/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 3M模型实例统计
 * @param data
 */
export function getSimulationReport(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/simulation/api/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 加签实例
 * @param data
 */
export function getNodeInstanceReport(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/node/api/inst/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 加签节点
 * @param data
 */
export function getNodeReport(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/report/node/api/node/list',
    method: 'post',
    data: qs.stringify(data)
  })
}

/**
 * 获取XML 列表
 * @param data
 */
export function getXmlParserReport(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/xml/parse/list/get',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取XML丢失列表
 * @param data
 */
export function getXmlCacheLoserReport(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/xml/parse/cache/loser/list/get',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取XML 解析数量
 * @param data
 */
export function getXmlParserCountReport(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/xml/report/parse/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
/**
 * 获取XML 解析数量
 * @param data
 */
export function getXmlCacheLoserCountReport(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM + '/a1bpmn/xml/report/cache/loser/list',
    method: 'post',
    data: qs.stringify(data)
  })
}
