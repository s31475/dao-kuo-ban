import request from '@/utils/request2'
import qs from 'qs'
import {Login,userInfo,GET_RESOURCE3,SERVICE_ADMIN,SERVICE_FORM,SERVICE_BPM} from './api'

/**
 * 加载数据
 * @param data
 */
export function getAllDef(data) {
  return request({
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    url: SERVICE_BPM+'/a1bpmn/api/runtime/def/v1/def/list',
    method: 'POST',
    data: qs.stringify(data)
  })
}

