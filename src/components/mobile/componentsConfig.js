export const basicComponents = [ // 默认表单组成的配置
  {
    type: 'input',
    name: '单行文本',
    controlType: '',
    icon: 'icon-input',
    options: {
      show: true,
      showTitle: true,
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      unit: '',
      dateCalculation: false,
      showPassword: false,
      dateCalculationStart: '',
      dateCalculationEnd: '',
      dateCalcDiffType: 'day',
      width: '70%',
      defaultValue: '',
      required: false,
      dataType: 'string',
      span: 24,
      pattern: '',
      placeholder: '',
      customClass: '',
      customStyle: '',
      disabled: false,
      showRed: false
    }
  },
  {
    type: 'textarea',
    name: '多行文本',
    controlType: '',
    icon: 'icon-diy-com-textarea',
    options: {
      dataType: 'string',
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      show: true,
      showTitle: true,
      dateCalculation: false,
      dateCalculationStart: '',
      dateCalculationEnd: '',
      dateCalcDiffType: 'day',
      width: '70%',
      defaultValue: '',
      span: 24,
      required: false,
      disabled: false,
      pattern: '',
      placeholder: '',
      customClass: '',
      showRed: false
    }
  },
  {
    type: 'select',
    name: '下拉选择框',
    icon: 'icon-select',
    controlType: '',
    options: {
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      show: true,
      showTitle: true,
      defaultValue: '',
      dateCalculation: false,
      dateCalcDiffType: 'day',
      dateCalculationStart: '',
      dateCalculationEnd: '',
      span: 24,
      disabled: false,
      clearable: false,
      placeholder: '',
      required: false,
      showLabel: false,
      width: '',
      options: [
        {
          value: '下拉框1'
        },
        {
          value: '下拉框2'
        }
      ],
      effect: [],
      remote: false,
      filterable: false,
      remoteOptions: [],
      props: {
        value: '',
        label: ''
      },
      remoteFunc: '',
      remoteUrlKey: '',
      customClass: '',
      showRed: false

    }
  },

  {
    type: 'radio',
    name: '单选',
    controlType: '',
    dateCalcDiffType: 'day',
    icon: 'icon-radio-active',
    options: {
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      show: true,
      showTitle: true,
      inline: true,
      defaultValue: '',
      showLabel: false,
      span: 24,
      controlType: '',
      dateCalculation: false,
      dateCalcDiffType: 'day',
      dateCalculationStart: '',
      dateCalculationEnd: '',

      options: [
        {
          value: '选项1',
          label: '选项1'
        },
        {
          value: '选项2',
          label: '选项2'
        }
      ],
      effect: [],
      required: false,
      border: false,
      disabled: false,
      width: '',
      remote: false,
      remoteOptions: [],
      remoteUrlKey: '',
      props: {
        value: 'value',
        label: 'label'
      },
      remoteFunc: '',
      customClass: '',
      showRed: false

    }
  },
  {
    type: 'checkbox',
    name: '多选',
    icon: 'icon-check-box',
    options: {
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      show: true,
      showTitle: true,
      span: 24,
      inline: true,
      disabled: false,
      dateCalculation: false,
      dateCalcDiffType: 'day',
      dateCalculationStart: '',
      dateCalculationEnd: '',
      effect: [],
      defaultValue: [],
      defaultValues: [],
      showLabel: false,
      options: [
        {
          value: '选项1'
        },
        {
          value: '选项2'
        }
      ],
      required: false,
      width: '',
      remote: false,
      remoteOptions: [],
      props: {
        value: 'value',
        label: 'label'
      },
      remoteUrlKey: '',
      remoteFunc: '',
      customClass: ''
    }
  },
  {
    type: 'date',
    name: '日期选择器',
    icon: 'icon-date',
    controlType: '',
    options: {
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      show: true,
      showTitle: true,
      defaultValue: '',
      dataType: 'string',
      dateValidRule: '',
      ckeckedDatevalidType: '',
      readonly: false,
      span: 24,
      disabled: false,
      editable: true,
      clearable: true,
      placeholder: '',
      startPlaceholder: '',
      endPlaceholder: '',
      type: 'datetime',
      format: 'yyyy-MM-dd HH:mm:ss',
      'value-format': 'yyyy-MM-dd HH:mm:ss',
      timestamp: true,
      required: false,
      width: '',
      customClass: '',
      showRed: false

    }
  },

  {
    type: 'fileUpload',
    name: '文件上传',
    icon: 'icon-shangchuan',
    options: {
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      show: true,
      showTitle: true,
      sizes: {
        width: 100,
        height: 100
      },
      width: 100,
      span: 24,
      token: '',
      disabled: false,
      length: 8,
      multiple: false,
      supportedAllTypes: ['jpg', 'jpeg', 'png', 'bmp', 'pdf', 'PBG', 'BMP', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'rtf', 'txt', 'zip', 'rar', 'vsd', 'dwg'],
      supportedTypes: ['jpg', 'jpeg', 'png', 'bmp', 'pdf', 'PBG', 'BMP', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'rtf', 'txt', 'zip', 'rar', 'vsd', 'dwg'],
      supportedTypeStr: '',

      min: 0,
      editable: false,
      action: 'http://127.0.0.1:8080/fastflow-admin/a1bpmn/api/multiUpload/',
      customClass: '',
      showRed: false
    }
  }

]

export const advanceComponents = [
  // {
  //   type: 'blank',
  //   name: '自定义',
  //   icon: 'icon-ic',
  //   options: {
  //     defaultType: 'String',
  //     customClass: '',
  //     width: ''
  //     ,showRed: false,
  //
  //   }
  // },

  // {
  //   type: 'editor',
  //   name: '富文本',
  //   icon: 'icon-fuwenbenkuang',
  //   options: {
  //     showAnnotation:false,
  //     annotationTitle:'添加批注',
  //     annotationContext:'',
  //     show: true,
  //     showTitle: true,
  //     defaultValue: '',
  //     width: '',
  //     span: 24,
  //     customClass: '',
  //     showRed: false,
  //     disabled: false,
  //
  //   }
  // },
  {
    type: 'vue2Editor',
    name: '富文本',
    icon: 'icon-fuwenbenkuang',
    options: {
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      show: true,
      showTitle: true,
      defaultValue: '',
      span: 24,
      customClass: '',
      action: '',
      disabled: false

    }
  },
  {
    type: 'table',
    name: '表格',
    icon: 'icon-table',
    options: {
      show: true,
      showTitle: true,
      defaultValues: [],
      customClass: '',
      span: 24,
      disabled: false

    },
    tableColumns: []
  }

]

export const layoutComponents = [
  {
    type: 'rate',
    name: '评分',
    icon: 'icon-icon-test',
    controlType: '',
    options: {
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      show: true,
      showTitle: true,
      defaultValue: null,
      max: 5,
      span: 24,
      disabled: false,
      allowHalf: false,
      required: false,
      customClass: '',
      showRed: false

    }
  },
  {
    type: 'tag',
    name: '标签',
    icon: 'icon-icon-test',
    controlType: '',
    options: {
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      show: true,
      showTitle: true,
      span: 24,
      defaultValue: '',
      radioSize: 'small',
      options: [
        {
          value: 'success',
          label: '标签一'
        },
        {
          value: 'info',
          label: '标签二'
        },
        {
          value: 'warning',
          label: '标签三'
        },
        {
          value: 'danger',
          label: '标签四'
        }
      ]

    }
  },
  {
    type: 'color',
    name: '颜色选择器',
    icon: 'icon-color',
    controlType: '',
    options: {
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      show: true,
      showTitle: true,
      defaultValue: '',
      span: 24,
      disabled: false,
      showAlpha: false,
      required: false,
      customClass: '',
      showRed: false

    }
  },

  {
    type: 'switch',
    name: '开关',
    icon: 'icon-switch',
    controlType: '',
    options: {
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      show: true,
      showTitle: true,
      defaultValue: false,
      span: 24,
      required: false,
      disabled: false,
      customClass: ''
    }
  }
]
export const navigationComponents = [
  {
    type: 'steps',
    name: '步骤条',
    icon: 'icon-slider',
    controlType: '',
    options: {
      showAnnotation: false,
      annotationTitle: '添加批注',
      annotationContext: '',
      span: 24,
      show: true,
      showTitle: true,
      defaultValue: 0,
      active: 0,
      nextStep: false,
      alignCenter: false,
      finishStatus: 'success',
      processStatus: 'success',
      direction: 'horizontal',
      space: 0,
      simple: false,
      stepItemOptions: [
        {
          value: '步骤1',
          description: ''
        },
        {
          value: '步骤2',
          description: ''
        },
        {
          value: '步骤3',
          description: ''
        }
      ]
    }
  }

]
