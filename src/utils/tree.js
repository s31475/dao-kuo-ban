export function transformTree(list, options = {}) {
  const {
    keyField = "id",
    childField = "children",
    parentField = "parentId"
  } = options;

  const tree = [];
  const record = {};

  for (let i = 0, len = list.length; i < len; i++) {
    if (!list[i].hasOwnProperty("id")) {
      tree.push(list[i]);
      continue;
    }
    const item = list[i];
    const id = item[keyField];

    if (!id) {
      continue;
    }

    if (record[id]) {
      item[childField] = record[id];
    } else {
      item[childField] = record[id] = [];
    }

    if (item[parentField]) {
      const parentId = item[parentField];

      if (!record[parentId]) {
        record[parentId] = [];
      }

      record[parentId].push(item);
    } else {
      tree.push(item);
    }
  }

  return tree;
}

let data = {
  "msg": "success",
  "code": 0,
  "data": [
      {
          "menuId": 1000,
          "parentId": 0,
          "children": null,
          "checked": false,
          "parentName": null,
          "expand": false,
          "id": "1000",
          "name": "首页",
          "title": "首页",
          "label": "首页",
          "url": "/indexPage",
          "perms": null,
          "type": 1,
          "icon": "fa fa-wpforms",
          "orderNum": 1,
          "menuLevel": 1,
          "deleted": false,
          "path": "/indexPage",
          "componentPath": "@/views/index-page/index",
          "componentName": "sy",
          "noCache": true,
          "hiden": false,
          "open": null,
          "list": null
      },
      {
          "menuId": 2000,
          "parentId": 0,
          "children": null,
          "checked": false,
          "parentName": null,
          "expand": false,
          "id": "2000",
          "name": "表单管理",
          "title": "表单管理",
          "label": "表单管理",
          "url": "/form",
          "perms": null,
          "type": 1,
          "icon": "fa fa-rocket",
          "orderNum": 2,
          "menuLevel": 1,
          "deleted": false,
          "path": "/form",
          "componentPath": "Layout",
          "componentName": "bdgl",
          "noCache": true,
          "hiden": false,
          "open": null,
          "list": null
      },
      {
          "menuId": 2100,
          "parentId": 2000,
          "children": null,
          "checked": false,
          "parentName": null,
          "expand": false,
          "id": "2100",
          "name": "业务表单",
          "title": "业务表单",
          "label": "业务表单",
          "url": "/formbus/businesstable/index",
          "perms": null,
          "type": 1,
          "icon": "fa fa-rocket",
          "orderNum": 2,
          "menuLevel": 2,
          "deleted": false,
          "path": "/formbus/businesstable/index",
          "componentPath": "Layout",
          "componentName": "ywbd",
          "noCache": true,
          "hiden": false,
          "open": null,
          "list": null
      },
      {
          "menuId": 21001,
          "parentId": 2100,
          "children": null,
          "checked": false,
          "parentName": null,
          "expand": false,
          "id": "21001",
          "name": "业务实体管理",
          "title": "业务实体管理",
          "label": "业务实体管理",
          "url": "/formbus/businesstable/index",
          "perms": null,
          "type": 1,
          "icon": "fa fa-rocket",
          "orderNum": 122,
          "menuLevel": 3,
          "deleted": false,
          "path": "/formbus/businesstable/index",
          "componentPath": "/formbus/businesstable/index",
          "componentName": "businesstableIndex",
          "noCache": true,
          "hiden": false,
          "open": null,
          "list": null
      },
      {
          "menuId": 21002,
          "parentId": 2100,
          "children": null,
          "checked": false,
          "parentName": null,
          "expand": false,
          "id": "21002",
          "name": "业务对象",
          "title": "业务对象",
          "label": "业务对象",
          "url": "/formbus/businessobject/index",
          "perms": null,
          "type": 1,
          "icon": "fa fa-rocket",
          "orderNum": 2,
          "menuLevel": 3,
          "deleted": false,
          "path": "/formbus/businessobject/index",
          "componentPath": "/formbus/businessobject/index",
          "componentName": "businessObjIndex",
          "noCache": true,
          "hiden": false,
          "open": null,
          "list": null
      },
      {
          "menuId": 21003,
          "parentId": 2100,
          "children": null,
          "checked": false,
          "parentName": null,
          "expand": false,
          "id": "21003",
          "name": "表单模型",
          "title": "表单模型",
          "label": "表单模型",
          "url": "/form/design/index",
          "perms": null,
          "type": 1,
          "icon": "fa fa-rocket",
          "orderNum": 3,
          "menuLevel": 3,
          "deleted": false,
          "path": "/form/design/index",
          "componentPath": "@/views/formbus/design/index",
          "componentName": "designForm",
          "noCache": true,
          "hiden": false,
          "open": null,
          "list": null
      },
      {
          "menuId": 21004,
          "parentId": 2100,
          "children": null,
          "checked": false,
          "parentName": null,
          "expand": false,
          "id": "21004",
          "name": "对话框管理",
          "title": "对话框管理",
          "label": "对话框管理",
          "url": "/form/customDialog/index",
          "perms": null,
          "type": 1,
          "icon": "fa fa-rocket",
          "orderNum": 4,
          "menuLevel": 3,
          "deleted": false,
          "path": "/form/customDialog/index",
          "componentPath": "@/views/formbus/customdialog/index",
          "componentName": "customDialogList",
          "noCache": true,
          "hiden": false,
          "open": null,
          "list": null
      }
  ]
}

/**
 * 树转数组扁平化结构
 * 深度优先遍历  递归
 */
export function deepTraversal(data) {
  const result = [];
  debugger;
  data.forEach(item => {
    const loop = data => {
      result.push({
        id: data.id,
        name: data.name,
        parentId: data.parentId,
        path: data.path,
        componentPath: data.componentPath,
        menuLevel: data.menuLevel
      });
      let child = data.children;
      if (child) {
        for (let i = 0; i < child.length; i++) {
          loop(child[i]);
        }
      }
    };
    loop(item);
  });
  return result;
}

/**
 * 树转数组扁平化结构
 * 深度优先遍历  递归
 */
export function deepTraversalLocal(data) {
  const result = [];
  data &&
    data.length &&
    data.forEach(item => {
      const loop = data => {
        result.push({
          ...data
        });
        let child = data.children;
        if (child && child.length > 0) {
          for (let i = 0; i < child.length; i++) {
            loop(child[i]);
          }
        }
      };
      loop(item);
    });
  return result;
}

/**
 * 广度优先
 * 队列  先进先出
 */
export function wideTraversal(node) {
  let stack = node,
    data = [];
  while (stack.length != 0) {
    let shift = stack.shift();
    data.push({
      id: shift.id,
      name: shift.name,
      parentId: shift.parentId
    });
    let children = shift.children;
    if (children) {
      for (let i = 0; i < children.length; i++) {
        stack.push(children[i]);
      }
    }
  }
  return data;
}


// 递归拼接树形菜单
export function arr2TreeMenus(menus) {
  const result = [];
  const itemMap = {};
  const action = [];
  let filterMenus = menus.filter(v => {
    return v.deleted == false;
  })

  console.log('_____________________arr2TreeMenus', filterMenus);
  for (const item of filterMenus) {
    const id = item.id;
    const pid = item.parentId;

    if (!itemMap[id]) {
      itemMap[id] = {
        children: [],
      };
    }

    itemMap[id] = {
      path: item.path,
      name: item.componentName,
      component: () => item.componentPath,
      hidden: item.hiden,
      meta: {
        title: item.title,
        icon: item.icon,
      },
      children: itemMap[id]["children"],
    };

    const treeItem = itemMap[id];

    if (pid === 0) {
      result.push(treeItem);
    } else {
      if (!itemMap[pid]) {
        itemMap[pid] = {
          children: [],
        };
      }
      itemMap[pid].children.push(treeItem);
      if (item.type == 1) {
        // itemMap[pid].children.push(treeItem);
      } else {
        action.push(treeItem);
      }
    }
  }
  console.log('arr2TreeMenus action', action);
  return result;
}
