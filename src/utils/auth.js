import Cookies from 'js-cookie'

const TokenKey = 'dkb_token'
const RoleTokenKey = 'Token-Role'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

// 设置角色
export function getRoleToken() {
  return Cookies.get(RoleTokenKey)
}

export function setRoleToken(token) {
  return Cookies.set(RoleTokenKey, token)
}

export function removeRoleToken() {
  return Cookies.remove(RoleTokenKey)
}
