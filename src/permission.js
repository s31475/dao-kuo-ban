import router from './router'
import store from './store'
import { Message } from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken, getRoleToken } from '@/utils/auth' // get token from cookie
import getPageTitle from '@/utils/get-page-title'

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ['/login','/register'] // no redirect whitelist

router.beforeEach(async(to, from, next) => {
  // start progress bar
  console.log('from', from.path)
  localStorage.setItem('from', from.path);
  NProgress.start()
  console.log("router.beforeEach to......", to, router)
  if(to.path === '/register'){
    document.title = to.meta.title
  }else{
    // set page title
    document.title = getPageTitle(to.meta.title)
  }


  // determine whether the user has logged in
  const hasToken = getToken()
  // has token
  if (hasToken) {
    if (to.path === '/login') {// 跳转登录
      console.log('if............', router);
      // next({ path: '/' })
      next()
      console.log('into next()1');
      NProgress.done()
    } else {// 登陆后
      console.log('else............1', store.state.permission);
      const hasRoles = store.getters.roles && store.getters.roles.length > 0;
      // const hasRoles = getRoleToken() && getRoleToken().length >0 ;
      if (hasRoles) {
        console.log('else............if', store.state, store.getters);
        if (from.path == "/login") {
          console.log('hasRoles', store.getters);
          await store.dispatch('user/getInfo');
          let roleArr = [];
          roleArr.push(store.getters.roles)
          const accessRoutes = await store.dispatch(
            "permission/generateRoutes",
            roleArr
          );
          router.addRoutes(accessRoutes);
          console.log('********* router', router);
          console.log('********* accessRoutes', accessRoutes);
        }
        // next({ ...to, replace: true })
        next();
      } else {
        try {
          // get user info
          const { roles } = await store.dispatch('user/getInfo');
          // const accessRoutes = await store.dispatch('permission/generateRoutes', menus)

          console.log('try1', store.getters, roles);
          const accessRoutes = await store.dispatch(
            "permission/generateRoutes",
            roles
          );
          console.log('try2', accessRoutes);
          router.addRoutes(accessRoutes)
          console.log('src permission', accessRoutes, router);
          next({ ...to, replace: true })
          console.log('into next()3');
        } catch (error) {
          console.log('catch', error);
          console.log('catch', store.state, store.getters);
          Message.error(error || 'Has Error')
          next(`/login?redirect=${to.path}`);
          console.log('into next()4');
          NProgress.done()
        }
      }
    }
  } else {
    // const accessRoutes = await store.dispatch('permission/generateRoutes')
    // router.addRoutes(accessRoutes)
    // next()
    // NProgress.done()
    /* has no token*/
    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
      console.log('into next()5');
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      console.log('into next()6');
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
