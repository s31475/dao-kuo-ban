import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
import commonLayout from '@/layout/commonLayout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
// 公共页面
export const constantRoutes = [

  // 首页
  {
    path: '/',
    component: Layout,
    redirect: '/indexPage',
    meta: { title: '工作台', icon: 'dashboard' },
    children: [
      {
        path: 'indexPage',
        name: 'indexPage',
        component: () => import('@/views/index-page/index.vue'),
        meta: { title: '', icon: 'el-icon-s-home' }
      }
    ]
  },

  {
    path: '/house',
    component: Layout,
    redirect: '/house/layout_info/house_info_input',
    alwaysShow: true,
    meta: { title: '房源信息管理子系统', icon: '' },
    children: [
      {
        path: 'layout_info',
        name: 'layout_info',
        redirect: 'noRedirect',
        component: () => import('@/views/house/index'),
        meta: { title: '配件政府性房源信息管理', icon: '' },
        children: [
          {
            path: 'house_info_input',
            name: 'house_info_input',
            component: () => import('@/views/house/info/Input/index.vue'),
            meta: { title: '开发商录入', icon: '' }
          },
          {
            path: 'house_info_receive',
            name: 'house_info_receive',
            component: () => import('@/views/house/info/receive/index.vue'),
            meta: { title: '房源接收', icon: '' }
          },
          {
            path: 'house_info_bookBuild',
            name: 'house_info_bookBuild',
            component: () => import('@/views/house/info/bookBuild/index.vue'),
            meta: { title: '房源建册', icon: '' }
          },
          {
            path: 'house_info_approval',
            name: 'house_info_approval',
            component: () => import('@/views/house/info/approve/index.vue'),
            meta: { title: '综合信息审批', icon: '' },
          },
          {
            path: 'house_info_approval_history',
            name: 'house_info_approval_history',
            component: () => import('@/views/house/info/approve/history.vue'),
            meta: { title: '综合信息审批历史', icon: '' },
          },
          {
            path: 'house_info_approval_details',
            name: 'house_info_approval_details',
            hidden: true,
            component: () => import('@/views/house/info/approve/details.vue'),
            meta: { title: '房源信息审核详情', icon: '', activeMenu: '/house/layout_info/house_info_approval' }
          },
          {
            path: 'house_info_approval_details_history',
            name: 'house_info_approval_details_history',
            hidden: true,
            component: () => import('@/views/house/info/approve/detailsHistory.vue'),
            meta: { title: '房源信息审核详情', icon: '', activeMenu: '/house/layout_info/house_info_approval_history' }
          },
          {
            path: 'house_info_approval_book_build_approve',
            name: 'house_info_approval_book_build_approve',
            hidden: true,
            component: () => import('@/views/house/info/approve/bookBuildApprove.vue'),
            meta: { title: '房源信息审核详情', activeMenu: '/house/layout_info/house_info_approval' }
          },
          {
            path: 'house_info_approval_book_build_approve_history',
            name: 'house_info_approval_book_build_approve_history',
            hidden: true,
            component: () => import('@/views/house/info/approve/bookBuildApproveHistory.vue'),
            meta: { title: '房源信息审核详情', activeMenu: '/house/layout_info/house_info_approval_history' }
          },
          {
            path: 'house_info_approval_area_approve',
            name: 'house_info_approval_area_approve',
            hidden: true,
            component: () => import('@/views/house/info/approve/areaApprove.vue'),
            meta: { title: '房源信息审核详情', activeMenu: '/house/layout_info/house_info_approval' }
          },
          {
            path: 'house_info_approval_area_approve_history',
            name: 'house_info_approval_area_approve_history',
            hidden: true,
            component: () => import('@/views/house/info/approve/areaApproveHistory.vue'),
            meta: { title: '房源信息审核详情', activeMenu: '/house/layout_info/house_info_approval_history' }
          },
        ]
      },
      {
        path: 'layout_Warehouse',
        name: 'layout_Warehouse',
        redirect: 'noRedirect',
        component: () => import('@/views/house/index'),
        meta: { title: '房源入库管理', icon: '' },
        children: [
          {
            path: 'warehouse_put_into_approve_index',
            name: 'house_warehouse_approve', // 房源信息审核 和 入库审核 和 入库房源修改审批 同一个页面
            component: () => import('@/views/house/Warehouse/putIntoApprove/index.vue'),
            meta: { title: '入库审核', icon: '' }
          },
          {
            path: 'warehouse_put_into_approve_history',
            name: 'house_warehouse_into_history', // 房源信息审核 和 入库审核 和 入库房源修改审批 同一个页面
            component: () => import('@/views/house/Warehouse/putIntoApprove/history.vue'),
            meta: { title: '入库审核历史', icon: '' }
          },
          {
            path: 'warehouse_warehouse_edit_approve_history',
            name: 'house_warehouse_edit_history', // 房源信息审核 和 入库审核 和 入库房源修改审批 同一个页面
            component: () => import('@/views/house/Warehouse/editApprove/history.vue'),
            meta: { title: '入库房源修改历史', icon: '' }
          },
          {
            path: 'warehouse_put_into_approve_history_detail',
            name: 'house_warehouse_into_history_detail', // 房源信息审核 和 入库审核 和 入库房源修改审批 同一个页面
            hidden: true,
            component: () => import('@/views/house/Warehouse/putIntoApprove/historyDetail.vue'),
            meta: { title: '入库审核历史详情', icon: '' }
          },
          {
            path: 'warehouse_edit_approve_history_detail',
            name: 'house_warehouse_edit_history_detail', // 房源信息审核 和 入库审核 和 入库房源修改审批 同一个页面
            component: () => import('@/views/house/Warehouse/editApprove/historyDetail.vue'),
            meta: { title: '入库房源修改历史详情', icon: '' }
          },
          {
            path: 'put_into_approve_details',
            name: 'put_into_approve_details', // 房源信息审核详情 和 入库审核详情 和 入库房源修改审批 同一个页面
            hidden: true,
            component: () => import('@/views/house/Warehouse/putIntoApprove/approve.vue'),
            meta: { title: '入库审核详情', icon: '', activeMenu: '/house/layout_Warehouse/warehouse_put_into_approve_index' }
          },
          {
            path: 'house_warehouse_info',
            name: 'house_warehouse_info',
            component: () => import('@/views/house/Warehouse/info/index.vue'),
            meta: { title: '入库房源', icon: '' },
          },
          {
            path: 'house_info_details',
            name: 'house_info_details',
            hidden: true,
            component: () => import('@/views/house/Warehouse/info/houseInfoDetails.vue'),
            meta: { title: '房源详情', icon: '', activeMenu: '/house/layout_Warehouse/house_warehouse_info' }
          },
          {
            path: 'warehouse_edit_approve_index',
            name: 'warehouse_change_approve', // 房源信息审核 和 入库审核 和 入库房源修改审批 同一个页面
            component: () => import('@/views/house/Warehouse/editApprove/index.vue'),
            meta: { title: '入库房源修改审批', icon: '' }
          },
          {
            path: 'warehouse_edit_approve',
            name: 'warehouse_edit_approve',
            hidden: true,
            component: () => import('@/views/house/Warehouse/editApprove/approve.vue'),
            meta: { title: '入库房源修改审批详情', icon: '', activeMenu: '/house/layout_Warehouse/warehouse_edit_approve_index' }
          }
        ]
      },
      {
        path: 'layout_community',
        name: 'layout_community',
        redirect: 'noRedirect',
        alwaysShow: true,
        component: () => import('@/views/house/index'),
        meta: { title: '小区信息管理', icon: '' },
        children: [{
          path: 'house_community_info',
          name: 'house_community_info',
          component: () => import('@/views/house/community/info/index.vue'),
          meta: { title: '小区信息管理', icon: '' }
        },{
          path: 'house_community_info_details',
          name: 'house_community_info_details',
          hidden: true,
          component: () => import('@/views/house/community/info/communityInfoDetails.vue'),
          meta: { title: '小区信息详情', icon: '', activeMenu: '/house/layout_community/house_community_info' }
        }]
      },
      {
        path: 'layout_standingBook',
        name: 'layout_standingBook',
        redirect: 'noRedirect',
        component: () => import('@/views/house/index'),
        meta: { title: '安置房台账管理', icon: '' },
        children: [{
          path: 'house_standingBook_own',
          name: 'house_standingBook_own',
          component: () => import('@/views/house/standingBook/Own/index.vue'),
          meta: { title: '自有物业台账', icon: '' }
        },{
          path: 'house_standingBook_region',
          name: 'house_standingBook_region',
          component: () => import('@/views/house/standingBook/region/index.vue'),
          meta: { title: '各区拆迁台账', icon: '' }
        },{
          path: 'house_standingBook_community',
          name: 'house_standingBook_community',
          component: () => import('@/views/house/standingBook/community/index.vue'),
          meta: { title: '小区台账', icon: '' }
        },
        // {
        //   path: 'house_standingBook_jiwai',
        //   name: 'house_standingBook_jiwai',
        //   component: () => import('@/views/house/standingBook/jiwai/index.vue'),
        //   meta: { title: '机外台账', icon: '' }
        // },
        // {
        //   path: 'house_standingBook_lease',
        //   name: 'house_standingBook_lease',
        //   component: () => import('@/views/house/standingBook/lease/index.vue'),
        //   meta: { title: '经营性租赁台账', icon: '' }
        // },
        {
          path: 'house_standingBook_search',
          name: 'house_standingBook_search',
          component: () => import('@/views/house/standingBook/search/index.vue'),
          meta: { title: '查冊台账', icon: '' }
        }]
      },
      {
        path: 'layout_statistics',
        name: 'layout_statistics',
        redirect: 'noRedirect',
        component: () => import('@/views/house/index'),
        meta: { title: '数据统计', icon: '' },
        children: [
          {
            path: 'charts',
            name: 'charts',
            component: () => import('@/views/house/info/statistics/charts.vue'),
            meta: { title: '统计', icon: '' }
          },
          // {
          //   path: 'house_info_NoStatistics',
          //   name: 'house_info_NoStatistics',
          //   component: () => import('@/views/house/info/statistics/index.vue'),
          //   meta: { title: '未建册数据统计', icon: '' }
          // },
          // {
          //   path: 'house_info_YesStatistics',
          //   name: 'house_info_YesStatistics',
          //   component: () => import('@/views/house/info/statistics/index.vue'),
          //   meta: { title: '已建册数据统计', icon: '' }
          // }
        ]
      }
    ]
  },

  {
    path: '/setting',
    meta: {
      title: '设置'
    },
    component: Layout,
    children: [

  // 用户中心
  {
    path: '/uc',
    name: '用户中心',
    component: commonLayout,
    sort: 4,
    redirect: '/uc/org/user',
    meta: { title: '组织管理', icon: '', noCache: true },
    children: [
      {
        path: '/uc/org/user',
        component: () => import('@/views/uc/user/index'),
        name: 'user_index',
        meta: { title: '人员管理', icon: '', noCache: true }
      },
      {
        path: '/uc/user/add',
        component: () => import('@/views/uc/user/add'),
        name: 'user_add',
        hidden: true,
        meta: { title: '人员信息', icon: '', noCache: true }
      },
      {
        path: '/uc/role/index',
        component: () => import('@/views/uc/role/index'),
        name: 'role_index',
        meta: { title: '角色管理', icon: '', noCache: true }
      },
      {
        path: '/uc/role/add',
        component: () => import('@/views/uc/role/add'),
        name: 'role_add',
        hidden: true,
        meta: { title: '角色管理', icon: '', noCache: true }
      },
      {
        path: '/uc/role/userList',
        component: () => import('@/views/uc/role/userList'),
        name: 'role_userList',
        hidden: true,
        meta: { title: '角色人员管理', icon: '', noCache: true }
      },
      {
        path: '/uc/ucorgjob/index',
        component: () => import('@/views/uc/ucorgjob/index'),
        name: 'ucorgjob_index',
        meta: { title: '职务管理', icon: '', noCache: true }
      },
      {
        path: '/uc/ucorgjob/add',
        component: () => import('@/views/uc/ucorgjob/add'),
        name: 'ucorgjob_add',
        hidden: true,
        meta: { title: '职务管理', icon: '', noCache: true }
      },
      {
        path: '/uc/demension/index',
        component: () => import('@/views/uc/demension/index'),
        name: 'demension_index',
        meta: { title: '组织维度管理', icon: '', noCache: true }
      },
      {
        path: '/uc/demension/add',
        component: () => import('@/views/uc/demension/add'),
        name: 'demension_add',
        hidden: true,
        meta: { title: '组织维度管理', icon: '', noCache: true }
      },
      {
        path: '/uc/ucorgpost/index',
        component: () => import('@/views/uc/ucorgpost/index'),
        name: 'ucorgpost_index',
        meta: { title: '岗位管理', icon: '', noCache: true }
      },
      {
        path: '/uc/ucorgpost/add',
        component: () => import('@/views/uc/ucorgpost/add'),
        name: 'ucorgpost_add',
        hidden: true,
        meta: { title: '岗位管理', icon: '', noCache: true }
      },
      {
        path: '/uc/ucorgpost/userList',
        component: () => import('@/views/uc/ucorgpost/userList'),
        name: 'ucorgpost_userList',
        hidden: true,
        meta: { title: '岗位人员管理', icon: '', noCache: true }
      },
      {
        path: '/uc/org/index',
        component: () => import('@/views/uc/org/index'),
        name: 'org_index',
        meta: { title: '组织管理', icon: '', noCache: true }
      },
      {
        path: '/uc/org/add',
        component: () => import('@/views/uc/org/add'),
        name: 'org_add',
        hidden: true,
        meta: { title: '组织管理', icon: '', noCache: true }
      },
      {
        path: '/uc/org/orgPost',
        component: () => import('@/views/uc/org/orgPost'),
        name: 'org_orgPost',
        hidden: true,
        meta: { title: '组织下属岗位', icon: '', noCache: true }
      },
      {
        path: '/uc/org/addUser',
        component: () => import('@/views/uc/org/addUser'),
        name: 'org_addUser',
        hidden: true,
        meta: { title: '添加人员', icon: '', noCache: true }
      },
      {
        path: '/uc/org/userList',
        component: () => import('@/views/uc/org/userList'),
        name: 'org_userList',
        hidden: true,
        meta: { title: '添加下属人员', icon: '', noCache: true }
      },
      {
        path: '/uc/org/addOrgPostUser',
        component: () => import('@/views/uc/org/addOrgPostUser'),
        name: 'org_addOrgPostUser',
        hidden: true,
        meta: { title: '组织添加人员', icon: '', noCache: true }
      }
    ],
    hidden: false
  },
  // 业务表单
  {
    path: '/form',
    name: '业务表单',
    component: commonLayout,
    sort: 2,
    redirect: '/formbus/businesstable/index',
    meta: { title: '业务表单', icon: '', noCache: true },
    children: [
      {
        path: '/formbus/design/app',
        component: () => import('@/views/formbus/design/app'),
        name: 'formModelerIndex',
        meta: { title: '表单设计器', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/formbus/design/modeler',
        component: () => import('@/views/formbus/design/modeler'),
        name: 'formModeler',
        meta: { title: '表单设计器', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/formbus/design/versionManager',
        component: () => import('@/views/formbus/design/versionManager'),
        name: 'formVersionManager',
        meta: { title: '版本管理', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/formbus/businesstable/index',
        component: () => import('@/views/formbus/businesstable/index'),
        name: 'businesstableIndex',
        meta: { title: '业务实体管理', icon: '', noCache: true }
      },
      {
        path: '/formbus/businesstable/inner/index',
        component: () => import('@/views/formbus/businesstable/inner/index'),
        name: 'businessTableInnerAdd',
        meta: { title: '新增业务实体', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/formbus/businessobject/index',
        component: () => import('@/views/formbus/businessobject/index'),
        name: 'businessObjIndex',
        meta: { title: '业务对象', icon: '', noCache: true }
      },
      {
        path: '/form/design/index',
        component: () => import('@/views/formbus/design/index'),
        name: 'designForm',
        meta: { title: '表单模型', icon: '', noCache: true }
      },
      {
        path: '/form/customDialog/index',
        component: () => import('@/views/formbus/customdialog/index'),
        name: 'customDialogList',
        meta: { title: '对话框管理', icon: '', noCache: true }
      },
      {
        path: '/formbus/businessobject/add',
        component: () => import('@/views/formbus/businessobject/add'),
        name: 'businessObjAdd',
        meta: { title: '新增业务对象', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/formbus/customdialog/add',
        component: () => import('@/views/formbus/customdialog/add'),
        name: 'customdialogAdd',
        meta: { title: '对话框', icon: '', noCache: true },
        hidden: true
      }
    ],
    hidden: false
  },
  // 流程管理
  {
    path: '/commu1',
    name: '流程管理',
    sort: 7,
    component: () => import('@/views/bpm/bpmLayout'),
    meta: { title: '流程管理', icon: '', noCache: true },
    children: [
      {
        path: '/task',
        name: '我的事项',
        sort: 7,
        component: commonLayout,
        redirect: '/bpm/task/index',
        meta: { title: '我的事项', icon: '', noCache: true },
        children: [
          {
            path: '/bpm/index',
            component: () => import('@/views/bpm/index'),
            name: 'bpmIndex',
            meta: { title: '首页', icon: '', noCache: true }
          },
          {
            path: '/bpm/task/index',
            component: () => import('@/views/bpm/task/index'),
            name: 'myTask',
            meta: { title: '我的任务', icon: '', noCache: true }
          },
          {
            path: '/bpm/task/timeout',
            component: () => import('@/views/bpm/task/timeout'),
            name: 'timeoutMyTask',
            meta: { title: '超时任务', icon: '', noCache: true }
          },
          {
            path: '/bpm/histask/index',
            component: () => import('@/views/bpm/histask/index'),
            name: 'histaskIndex',
            meta: { title: '已办任务', icon: '', noCache: true }
          },
          {
            path: '/bpm/task/handleTask',
            component: () => import('@/views/bpm/task/handle'),
            name: 'handleRunTask',
            meta: { title: '办理任务', icon: '', noCache: true },
            hidden: true
          },
          {
            path: 'bpm/histask/details',
            component: () => import('@/views/bpm/histask/details'),
            name: 'handleHisTaskDetails',
            meta: { title: '历史任务详情', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/bpm/wfmoduleIndex/ruIndex',
            component: () => import('@/views/bpm/wfmodule/index'),
            name: 'wfmoduleIndex',
            meta: { title: '发起流程', icon: '', noCache: true }
          },
          {
            path: '/bpm/wfmoduleIndex/start/ruIndex',
            component: () => import('@/views/bpm/wfmodule/start'),
            name: 'wfmoduleStart',
            meta: { title: '发起申请', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/bpm/wfmoduleIndex/start/draft',
            component: () => import('@/views/bpm/wfmodule/draft'),
            name: 'ru_draft',
            meta: { title: '草稿', icon: '', noCache: true }
          },
          {
            path: '/bpm/wfmoduleIndex/start/draftStart',
            component: () => import('@/views/bpm/wfmodule/draftStart'),
            name: 'draftStart',
            meta: { title: '发起申请', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/bpm/startup/ruIndex',
            component: () => import('@/views/bpm/startup/ruIndex'),
            name: 'startupRuIndex',
            meta: { title: '运行', icon: '', noCache: true }
          },
          {
            path: '/bpm/startup/hiIndex',
            component: () => import('@/views/bpm/startup/hiIndex'),
            name: 'hiIndex',
            meta: { title: '办结', icon: '', noCache: true }
          },
          {
            path: '/bpm/startup/runDetails',
            component: () => import('@/views/bpm/startup/runDetails'),
            name: 'runDetails',
            meta: { title: '任务明细', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/bpm/startup/hisDetails',
            component: () => import('@/views/bpm/startup/hisDetails'),
            name: 'hisDetails',
            meta: { title: '任务明细', icon: '', noCache: true },
            hidden: true
          }

        ],
        hidden: false
      },
      {
        path: '/finance',
        component: commonLayout,
        sort: 7,
        redirect: '/finance/index',
        meta: { title: '流程辅助功能', icon: '', noCache: true },
        children: [
          {
            path: '/modelIndex',
            component: () => import('@/views/bpm/model/index'),
            name: 'Finance',
            meta: { title: '模型管理', icon: '', noCache: true }
          },
          {
            path: '/bpm/model/v4',
            component: () => import('@/views/bpm/model/v4'),
            name: 'v4Index',
            meta: { title: '业务流程设计器', icon: '', noCache: true }
          },
          {
            path: '/low/bpmOftenFlow/index',
            component: () => import('@/views/low/bpmOftenFlow/index'),
            name: 'bpmOftenFlowLowIndex',
            meta: { title: '业务平台常用流程', icon: '', noCache: true }
          },
          {
            path: '/testBpm',
            component: () => import('@/views/bpm/model/testBpm'),
            name: 'testBpm',
            meta: { title: '自动化流程测试', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/low/bpmOftenFlow/add',
            component: () => import('@/views/low/bpmOftenFlow/add'),
            name: 'addV4BpmOftenFlow',
            meta: { title: '添加常用流程', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/diffModel',
            component: () => import('@/views/bpm/model/diffModel'),
            name: 'testBpm',
            meta: { title: '自动化测试(BPS平台)', icon: '', noCache: true }
          },
          {
            path: 'busModel',
            component: () => import('@/views/bpm/model/busModel'),
            name: 'busModel',
            meta: { title: '模型业务设置', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/bpm/bpmOftenFlow/index',
            component: () => import('@/views/bpm/bpmOftenFlow/index'),
            name: 'bpmOftenFlow',
            meta: { title: '常用流程', icon: '', noCache: true }
          },
          // {
          //   path: '/bpm/addBpmOftenFlow/index',
          //   component: () => import('@/views/bpm/bpmOftenFlow/add'),
          //   name: 'addBpmOftenFlow',
          //   meta: { title: '添加常用流程', icon: '', noCache: true },
          //   hidden: true
          // },
          {
            path: '/bpm/model/versionManager',
            component: () => import('@/views/bpm/model/versionManager'),
            name: 'versionManager',
            meta: { title: '版本管理', icon: '', noCache: true },
            hidden: true
          },

          {
            path: '/bpm/runtime/instance/v1/getInstanceLis',
            component: () => import('@/views/bpm/taskinstance/index'),
            name: 'Finance',
            meta: { title: '实例管理', icon: '', noCache: true }
          },
          {
            path: '/bpm/his/instance/resurrection',
            component: () => import('@/views/bpm/resurrection/index'),
            name: 'resurrection_instance',
            meta: { title: '历史实例复活', icon: '', noCache: true }
          },
          {
            path: '/bpm/his/instance/setting',
            component: () => import('@/views/bpm/resurrection/setting'),
            name: 'resurrection_instance_setting',
            meta: { title: '实例复活设置', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/bpm/model/urging',
            component: () => import('@/views/bpm/model/urging'),
            name: 'urging_task_setting',
            meta: { title: '催办设置', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/bpm/model/expire',
            component: () => import('@/views/bpm/model/expire'),
            name: 'expire_task_setting',
            meta: { title: '到期提醒', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/bpm/model/sequenceModel',
            component: () => import('@/views/bpm/model/sequenceModel'),
            name: 'sequenceModel_setting',
            meta: { title: '连线按钮', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/bpm/messageTemplate/list',
            component: () => import('@/views/bpm/messageTemplate/index'),
            name: 'Finance',
            meta: { title: '流程消息模板', icon: '', noCache: true }
          },
          {
            path: '/bpm/script/index',
            component: () => import('@/views/bpm/script/index'),
            name: 'Finance',
            meta: { title: '常用脚本管理', icon: '', noCache: true }
          },
          {
            path: '/bpm/agent/index',
            component: () => import('@/views/bpm/agent/index'),
            name: 'Finance',
            meta: { title: '委托设置', icon: '', noCache: true }
          }
        ]
      },
      {
        path: '/commu',
        name: '阅读事项',
        sort: 7,
        component: commonLayout,
        redirect: '/bpm/commu/ruIndex',
        meta: { title: '阅读事项', icon: '', noCache: true },
        children: [
          {
            path: '/bpm/commu/ruIndex',
            component: () => import('@/views/bpm/commu/ruIndex'),
            name: 'commuRuIndex',
            meta: { title: '待阅任务', icon: '', noCache: true }
          },
          {
            path: '/bpm/commu/hiIndex',
            component: () => import('@/views/bpm/commu/hiIndex'),
            name: 'commuHiIndex',
            meta: { title: '已阅任务', icon: '', noCache: true }
          },
          {
            path: '/bpm/commu/details',
            component: () => import('@/views/bpm/commu/details'),
            name: 'runCommuDetails',
            meta: { title: '待阅任务明细', icon: '', noCache: true },
            hidden: true
          },
          {
            path: '/bpm/commu/Hidetails',
            component: () => import('@/views/bpm/commu/hiDetails'),
            name: 'hiCommuDetails',
            meta: { title: '已阅任务明细', icon: '', noCache: true },
            hidden: true
          }
        ],
        hidden: false
      },
      {
        path: '/notify',
        name: '通知',
        sort: 7,
        component: commonLayout,
        redirect: '/bpm/msg/ruIndex',
        meta: { title: '通知事项', icon: '', noCache: true },
        children: [
          {
            path: '/bpm/msg/ruIndex',
            component: () => import('@/views/bpm/msg/ruIndex'),
            name: 'msgRuIndex',
            meta: { title: '未读消息', icon: '', noCache: true }
          },
          {
            path: '/bpm/msg/hiIndex',
            component: () => import('@/views/bpm/msg/hiIndex'),
            name: 'HiIndex',
            meta: { title: '已读消息', icon: '', noCache: true }
          }
        ],
        hidden: false
      },
    ]
  },
  {
    path: '/commu',
    name: '阅读事项',
    sort: 7,
    component: commonLayout,
    redirect: '/bpm/commu/ruIndex',
    meta: { title: '阅读事项', icon: '', noCache: true },
    children: [
      {
        path: '/bpm/commu/ruIndex',
        component: () => import('@/views/bpm/commu/ruIndex'),
        name: 'commuRuIndex',
        meta: { title: '待阅任务', icon: '', noCache: true }
      },
      {
        path: '/bpm/commu/hiIndex',
        component: () => import('@/views/bpm/commu/hiIndex'),
        name: 'commuHiIndex',
        meta: { title: '已阅任务', icon: '', noCache: true }
      },
      {
        path: '/bpm/commu/details',
        component: () => import('@/views/bpm/commu/details'),
        name: 'runCommuDetails',
        meta: { title: '待阅任务明细', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/bpm/commu/Hidetails',
        component: () => import('@/views/bpm/commu/hiDetails'),
        name: 'hiCommuDetails',
        meta: { title: '已阅任务明细', icon: '', noCache: true },
        hidden: true
      }
    ],
    hidden: false
  },
  {
    path: '/cmmn',
    name: '案例平台',
    component: commonLayout,
    sort: 6,
    redirect: '/cmmn/model/index',
    meta: { title: '案例平台', icon: '', noCache: true },
    children: [
      {
        path: '/cmmn/model/index',
        component: () => import('@/views/cmmn/model/index'),
        name: 'cmmn_model_index',
        meta: { title: '案例模型', icon: '', noCache: true }
      },
      {
        path: '/cmmn/model/versionManager',
        component: () => import('@/views/cmmn/model/versionManager'),
        name: 'cmmn_versionManager',
        meta: { title: '版本管理', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/cmmn/model/instanceList',
        component: () => import('@/views/cmmn/model/instanceList'),
        name: 'cmmn_instanceList',
        meta: { title: '案例实例管理', icon: '', noCache: true },
      },
      {
        path: '/cmmn/model/planItemList',
        component: () => import('@/views/cmmn/model/planItemList'),
        name: 'cmmn_planItemList',
        meta: { title: '运行计划项管理', icon: '', noCache: true },
      },
      {
        path: '/cmmn/model/runMilestoneList',
        component: () => import('@/views/cmmn/model/runMilestoneList'),
        name: 'cmmn_runMilestoneList',
        meta: { title: '运行里程碑实例管理', icon: '', noCache: true },
      },
      {
        path: '/cmmn/model/deployment',
        component: () => import('@/views/cmmn/model/deployment'),
        name: 'cmmn_deployment',
        meta: { title: '资源监控', icon: '', noCache: true },
      },
      {
        path: '/cmmn/model/cacheMonitor',
        component: () => import('@/views/cmmn/model/cacheMonitor'),
        name: 'cmmn_cacheMonitor',
        meta: { title: '案例缓存监控', icon: '', noCache: true },
      },
      {
        path: '/cmmn/model/hisInstanceList',
        component: () => import('@/views/cmmn/model/hisInstanceList'),
        name: 'cmmn_his_instanceList',
        meta: { title: '历史案例实例管理', icon: '', noCache: true },
      },
      {
        path: '/cmmn/model/hisPlanItemList',
        component: () => import('@/views/cmmn/model/hisPlanItemList'),
        name: 'cmmn_hisPlanItemList',
        meta: { title: '历史计划项管理', icon: '', noCache: true },
      },
      {
        path: '/cmmn/model/hisMilestoneList',
        component: () => import('@/views/cmmn/model/hisMilestoneList'),
        name: 'cmmn_hisMilestoneList',
        meta: { title: '历史里程碑实例管理', icon: '', noCache: true },
      },

    ],
    hidden: false
  },
  {
    path: '/monitor',
    name: '引擎监控',
    component: commonLayout,
    sort: 3,
    redirect: '/bpm/sql/list',
    meta: { title: '引擎监控', icon: '', noCache: true },
    children: [
      {
        path: '/bpm/sql/list',
        component: () => import('@/views/bpm/sql/index'),
        name: 'sql',
        meta: { title: '引擎Sql监控', icon: '', noCache: true }
      },
      {
        path: '/bpm/cmd/log/page/list',
        component: () => import('@/views/bpm/cmdlog/index'),
        name: 'cmdlogIndex',
        meta: { title: '引擎内核监控', icon: '', noCache: true }
      },
      {
        path: '/bpm/cmd/link/page/list',
        component: () => import('@/views/bpm/cmdlink/index'),
        name: 'cmdlinkIndex',
        meta: { title: '引擎数据链路', icon: '', noCache: true }
      },
      {
        path: '/bpm/monitor/index',
        component: () => import('@/views/bpm/monitor/index'),
        name: 'monitorIndex',
        meta: { title: '流程缓存监控', icon: '', noCache: true }
      },
      {
        path: '/bpm/cockpit/index',
        component: () => import('@/views/bpm/cockpit/index'),
        name: 'cockpitIndex',
        meta: { title: '流程优化管理', icon: '', noCache: true }
      },
      {
        path: '/bpm/cockpit/detail',
        component: () => import('@/views/bpm/cockpit/detail'),
        name: 'cockpitDetailIndex',
        meta: { title: '流程优化详情', icon: '', noCache: true },
        hidden: true
      },
      {
        path: '/bpm/monitor/instace/index',
        component: () => import('@/views/bpm/monitor/instace/index'),
        name: 'cockpitInstanceIndex',
        meta: { title: '流程实例监控', icon: '', noCache: true }
      },
      {
        path: '/bpm/monitor/task/index',
        component: () => import('@/views/bpm/monitor/task/index'),
        name: 'cockpitTaskIndex',
        meta: { title: '任务监控', icon: '', noCache: true }
      },
      {
        path: '/bpm/monitor/model/index',
        component: () => import('@/views/bpm/monitor/model/index'),
        name: 'cockpitModelIndex',
        meta: { title: 'BPM模型解析监控', icon: '', noCache: true }
      },
      {
        path: '/bpm/monitor/cache/index',
        component: () => import('@/views/bpm/monitor/cache/index'),
        name: 'cockpitCacheIndex',
        meta: { title: 'BPM模型缓存丢失监控', icon: '', noCache: true }
      },
      {
        path: '/bpm/monitor/engine/index',
        component: () => import('@/views/bpm/monitor/engine/index'),
        name: 'cockpitEngineIndex',
        meta: { title: 'BPM引擎监控', icon: '', noCache: true }
      }
    ],
    hidden: false
  },
  {
    path: '/notify',
    name: '通知',
    sort: 7,
    component: commonLayout,
    redirect: '/bpm/msg/ruIndex',
    meta: { title: '通知事项', icon: '', noCache: true },
    children: [
      {
        path: '/bpm/msg/ruIndex',
        component: () => import('@/views/bpm/msg/ruIndex'),
        name: 'msgRuIndex',
        meta: { title: '未读消息', icon: '', noCache: true }
      },
      {
        path: '/bpm/msg/hiIndex',
        component: () => import('@/views/bpm/msg/hiIndex'),
        name: 'HiIndex',
        meta: { title: '已读消息', icon: '', noCache: true }
      }
    ],
    hidden: false
  },
    ]
  },

  // 登录
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    meta: { title: 'login', icon: '' },
    hidden: true
  },
  // 注册
  {
    path: '/register',
    component: () => import('@/views/login/register'),
    meta: { title: '', icon: '', noCache: true },
    hidden: true
  },
  // 修改密码
  {
    path: '/ChangePassword',
    component: Layout,
    redirect: '/ChangePassword/change',
    alwaysShow: true,
    hidden: true,
    meta: { title: '修改密码', icon: 'el-icon-s-tools' },
    children: [{
      path: 'change',
      name: 'change',
      component: () => import('@/views/ChangePassword/index'),
      meta: { title: '修改密码', icon: '' }
    },]
  },
]

export const asyncRoutes = [
  // 2021.12.31 feng -------------------start
  // 空置房
  // {
  //   path: '/vacant_house',
  //   component: Layout,
  //   redirect: '/vacant_house/dashboard',
  //   alwaysShow: true,
  //   // 面包屑中不可点击
  //   redirect: 'noRedirect',
  //   meta: { title: '空置房安全管理系统', icon: '' },
  //   children: [
  //     {
  //       path: 'dashboard',
  //       name: 'dashboard',
  //       redirect: 'noRedirect',
  //       component: () => import('@/views/vacant_house/index'),
  //       meta: { title: '空置房入侵报警', icon: '' },
  //       children: [
  //           {
  //             path: 'index',
  //             name: 'index',
  //             component: () => import('@/views/vacant_house/dashboard/index'),
  //             meta: { title: '综合态势', icon: 'zonghe' }
  //         },{
  //             path: 'warn1',
  //             name: 'warn1',
  //             component: () => import('@/views/vacant_house/dashboard/gaojing'),
  //             meta: { title: '实时告警信息', icon: 'gaojing' }
  //         },{
  //             path: 'warn',
  //             name: 'warn',
  //             component: () => import('@/views/vacant_house/warn/index'),
  //             meta: { title: '历史告警信息', icon: 'gaojing' }
  //         },
  //       ]
  //     },{
  //       path: 'safety',
  //       name: 'safety',
  //       alwaysShow: true,
  //       component: () => import('@/views/vacant_house/index'),
  //       meta: { title: '空置房安全管理', icon: '' },
  //       children: [
  //           {
  //             path: 'mana',
  //             name: 'mana',
  //             component: () => import('@/views/vacant_house/mana/index'),
  //             meta: { title: '实时监控', icon: 'jiankongshexiangtou-xianxing' }
  //           },{
  //             path: 'mana_details',
  //             name: 'mana_details',
  //             hidden: true,
  //             component: () => import('@/views/vacant_house/mana/details'),
  //             meta: { title: '房屋详情', activeMenu: '/vacant_house/safety/mana' }
  //           }
  //       ]
  //     },{
  //       path: 'house_manage',
  //       name: 'house_manage',
  //       alwaysShow: true,
  //       component: () => import('@/views/vacant_house/index'),
  //       meta: { title: '房屋管理', icon: '' },
  //       children: [
  //           {
  //             path: 'houseInfo',
  //             name: 'houseInfo',
  //             component: () => import('@/views/vacant_house/house/index'),
  //             meta: { title: '房屋信息', icon: 'fangwuzixun' }
  //           }
  //       ]
  //     },{
  //       path: 'eq',
  //       name: 'eq',
  //       alwaysShow: true,
  //       component: () => import('@/views/vacant_house/index'),
  //       meta: { title: '设备管理', icon: '' },
  //       children: [
  //           {
  //             path: 'gateway',
  //             name: 'gateway',
  //             component: () => import('@/views/vacant_house/gateway/index'),
  //             meta: { title: '网关管理', icon: 'shebeiguanli' }
  //         },
  //         {
  //             path: 'binding_gateway',
  //             name: 'binding_gateway',
  //             hidden: true,
  //             component: () => import('@/views/vacant_house/gateway/binding_gateway'),
  //             meta: { title: '网关绑定', icon: 'shebeiguanli', activeMenu: '/vacant_house/eq/gateway' }
  //         },
  //         {
  //             path: 'facility',
  //             name: 'facility',
  //             component: () => import('@/views/vacant_house/facility/index'),
  //             meta: { title: '基础设备', icon: 'table' }
  //         },
  //         {
  //             path: 'factory',
  //             name: 'factory',
  //             component: () => import('@/views/vacant_house/factory/index'),
  //             meta: { title: '厂家管理', icon: 'table' }
  //         }
  //       ]
  //     },
  //   ]
  // },
  // 2021.12.31 feng ----------------------------------------------end
  // {
  //   path: '/setting1',
  //   meta: {
  //     title: '设置1'
  //   },
  //   component: Layout,
  //   children: [
  //     {
  //       path: '/setting21',
  //       meta: {
  //         title: '设置21'
  //       },
  //       component: layoutView,
  //       children: [
  //         {
  //           path: '/one1',
  //           meta: {
  //             title: '设置3'
  //           },
  //           component: layoutView,
  //           children: [
  //             {
  //               path: '/setting/one',
  //               meta: {
  //                 title: '设置1'
  //               },
  //               component: () => import('@/views/setting/one'),
  //             },
  //             {
  //               path: '/setting/two',
  //               meta: {
  //                 title: '设置2'
  //               },
  //               component: () => import('@/views/setting/two'),
  //             },
  //             {
  //               path: '/setting/three',
  //               meta: {
  //                 title: '设置3'
  //               },
  //               component: () => import('@/views/setting/three'),
  //             },
  //             {
  //               path: '/setting/four',
  //               meta: {
  //                 title: '设置4'
  //               },
  //               component: () => import('@/views/setting/four'),
  //             }
  //           ]
  //         }
  //       ]
  //     },
  //     {
  //       path: '/setting22',
  //       meta: {
  //         title: '设置22'
  //       },
  //       component: layoutView,
  //       children: [
  //         {
  //           path: '/one2',
  //           meta: {
  //             title: '设置3'
  //           },
  //           component: layoutView,
  //           children: [
  //             {
  //               path: '/setting/one1',
  //               meta: {
  //                 title: '设置1'
  //               },
  //               component: () => import('@/views/setting/one copy'),
  //             },
  //             {
  //               path: '/setting/two1',
  //               meta: {
  //                 title: '设置2'
  //               },
  //               component: () => import('@/views/setting/two copy'),
  //             },
  //             {
  //               path: '/setting/three1',
  //               meta: {
  //                 title: '设置3'
  //               },
  //               component: () => import('@/views/setting/three copy'),
  //             },
  //             {
  //               path: '/setting/four1',
  //               meta: {
  //                 title: '设置4'
  //               },
  //               component: () => import('@/views/setting/four copy'),
  //             }
  //           ]
  //         }
  //       ]
  //     }
  //   ]
  // },
  // 房源
  // {
  //   path: '/house',
  //   component: Layout,
  //   redirect: '/house/layout_info',
  //   alwaysShow: true,
  //   // 面包屑中不可点击
  //   redirect: 'noRedirect',
  //   meta: { title: '房源信息管理子系统', icon: '' },
  //   children: [
  //     {
  //       path: 'layout_info',
  //       name: 'layout_info',
  //       redirect: 'noRedirect',
  //       component: () => import('@/views/house/index'),
  //       meta: { title: '房源信息管理', icon: '' },
  //       children: [
  //         {
  //           path: 'house_info_input',
  //           name: 'house_info_input',
  //           component: () => import('@/views/house/info/Input/index.vue'),
  //           meta: { title: '开发商录入', icon: '' }
  //         },
  //         {
  //           path: 'house_info_receive',
  //           name: 'house_info_receive',
  //           component: () => import('@/views/house/info/receive/index.vue'),
  //           meta: { title: '房源接收', icon: '' }
  //         },
  //         {
  //           path: 'house_info_approval',
  //           name: 'house_info_approval',
  //           component: () => import('@/views/house/info/approve/index.vue'),
  //           meta: { title: '综合信息审批', icon: '' },
  //           children: [
  //             {
  //               path: 'house_info_approval_book_build_approve',
  //               name: 'house_info_approval_book_build_approve',
  //               hidden: true,
  //               component: () => import('@/views/house/info/approve/bookBuildApprove.vue'),
  //               meta: { title: '房源信息审核详情1', icon: '' }
  //             }
  //           ]
  //         },
  //         {
  //           path: 'house_info_approval_book_build_approve',
  //           name: 'house_info_approval_book_build_approve',
  //           hidden: true,
  //           component: () => import('@/views/house/info/approve/bookBuildApprove.vue'),
  //           meta: { title: '房源信息审核详情2', activeMenu: '/house/layout_info/house_info_approval' }
  //         },
  //         {
  //           path: 'house_info_approval_area_approve',
  //           name: 'house_info_approval_area_approve',
  //           hidden: true,
  //           component: () => import('@/views/house/info/approve/areaApprove.vue'),
  //           meta: { title: '房源信息审核详情3', activeMenu: '/house/layout_info/house_info_approval' }
  //         },
  //         {
  //           path: 'house_info_approval_details',
  //           name: 'house_info_approval_details',
  //           hidden: true,
  //           component: () => import('@/views/house/info/approve/details.vue'),
  //           meta: { title: '房源信息审核详情1', icon: '', activeMenu: '/house/layout_info/house_info_approval' }
  //         },
  //         {
  //           path: 'house_info_bookBuild',
  //           name: 'house_info_bookBuild',
  //           component: () => import('@/views/house/info/bookBuild/index.vue'),
  //           meta: { title: '房源建册', icon: '' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'layout_Warehouse',
  //       name: 'layout_Warehouse',
  //       redirect: 'noRedirect',
  //       component: () => import('@/views/house/index'),
  //       meta: { title: '房源入库管理', icon: '' },
  //       children: [
  //         {
  //           path: 'warehouse_put_into_approve_index',
  //           name: 'house_warehouse_approve', // 房源信息审核 和 入库审核 和 入库房源修改审批 同一个页面
  //           component: () => import('@/views/house/Warehouse/putIntoApprove/index.vue'),
  //           meta: { title: '入库审核', icon: '' }
  //         },
  //         {
  //           path: 'put_into_approve_details',
  //           name: 'put_into_approve_details', // 房源信息审核详情 和 入库审核详情 和 入库房源修改审批 同一个页面
  //           hidden: true,
  //           component: () => import('@/views/house/Warehouse/putIntoApprove/approve.vue'),
  //           meta: { title: '入库审核详情', icon: '', activeMenu: '/house/layout_Warehouse/warehouse_put_into_approve_index' }
  //         },
  //         {
  //           path: 'house_warehouse_info',
  //           name: 'house_warehouse_info',
  //           component: () => import('@/views/house/Warehouse/info/index.vue'),
  //           meta: { title: '入库房源', icon: '' },
  //         },
  //         {
  //           path: 'house_info_details',
  //           name: 'house_info_details',
  //           hidden: true,
  //           component: () => import('@/views/house/Warehouse/info/houseInfoDetails.vue'),
  //           meta: { title: '房源详情', icon: '', activeMenu: '/house/layout_Warehouse/house_warehouse_info' }
  //         },
  //         {
  //           path: 'warehouse_edit_approve_index',
  //           name: 'warehouse_change_approve', // 房源信息审核 和 入库审核 和 入库房源修改审批 同一个页面
  //           component: () => import('@/views/house/Warehouse/editApprove/index.vue'),
  //           meta: { title: '入库房源修改审批', icon: '' }
  //         },
  //         {
  //           path: 'warehouse_edit_approve',
  //           name: 'warehouse_edit_approve',
  //           hidden: true,
  //           component: () => import('@/views/house/Warehouse/editApprove/approve.vue'),
  //           meta: { title: '入库房源修改审批详情', icon: '', activeMenu: '/house/layout_Warehouse/warehouse_edit_approve_index' }
  //         }
  //       ]
  //     },
  //     {
  //       path: 'layout_community',
  //       name: 'layout_community',
  //       redirect: 'noRedirect',
  //       alwaysShow: true,
  //       component: () => import('@/views/house/index'),
  //       meta: { title: '小区信息管理', icon: '' },
  //       children: [{
  //         path: 'house_community_info',
  //         name: 'house_community_info',
  //         component: () => import('@/views/house/community/info/index.vue'),
  //         meta: { title: '小区信息管理', icon: '' }
  //       },{
  //         path: 'house_community_info_details',
  //         name: 'house_community_info_details',
  //         hidden: true,
  //         component: () => import('@/views/house/community/info/communityInfoDetails.vue'),
  //         meta: { title: '小区信息详情', icon: '', activeMenu: '/house/layout_community/house_community_info' }
  //       }]
  //     },
  //     {
  //       path: 'layout_standingBook',
  //       name: 'layout_standingBook',
  //       redirect: 'noRedirect',
  //       component: () => import('@/views/house/index'),
  //       meta: { title: '安置房台账管理', icon: '' },
  //       children: [{
  //         path: 'house_standingBook_own',
  //         name: 'house_standingBook_own',
  //         component: () => import('@/views/house/standingBook/Own/index.vue'),
  //         meta: { title: '自有物业台账', icon: '' }
  //       },{
  //         path: 'house_standingBook_region',
  //         name: 'house_standingBook_region',
  //         component: () => import('@/views/house/standingBook/region/index.vue'),
  //         meta: { title: '各区拆迁台账', icon: '' }
  //       },{
  //         path: 'house_standingBook_community',
  //         name: 'house_standingBook_community',
  //         component: () => import('@/views/house/standingBook/community/index.vue'),
  //         meta: { title: '小区台账', icon: '' }
  //       },{
  //         path: 'house_standingBook_jiwai',
  //         name: 'house_standingBook_jiwai',
  //         component: () => import('@/views/house/standingBook/jiwai/index.vue'),
  //         meta: { title: '机外台账', icon: '' }
  //       },{
  //         path: 'house_standingBook_lease',
  //         name: 'house_standingBook_lease',
  //         component: () => import('@/views/house/standingBook/lease/index.vue'),
  //         meta: { title: '经营性租赁台账', icon: '' }
  //       },{
  //         path: 'house_standingBook_search',
  //         name: 'house_standingBook_search',
  //         component: () => import('@/views/house/standingBook/search/index.vue'),
  //         meta: { title: '查冊台账', icon: '' }
  //       }]
  //     },
  //     {
  //       path: 'layout_statistics',
  //       name: 'layout_statistics',
  //       redirect: 'noRedirect',
  //       component: () => import('@/views/house/index'),
  //       meta: { title: '数据统计', icon: '' },
  //       children: [{
  //         path: 'house_info_NoStatistics',
  //         name: 'house_info_NoStatistics',
  //         component: () => import('@/views/house/info/statistics/index.vue'),
  //         meta: { title: '未建册数据统计', icon: '' }
  //       },{
  //         path: 'house_info_YesStatistics',
  //         name: 'house_info_YesStatistics',
  //         component: () => import('@/views/house/info/statistics/index.vue'),
  //         meta: { title: '已建册数据统计', icon: '' }
  //       }]
  //     }
  //   ]
  // },
  // 修改密码
  {
    path: '/ChangePassword',
    component: Layout,
    redirect: '/ChangePassword/change',
    alwaysShow: true,
    hidden: true,
    meta: { title: '修改密码', icon: 'el-icon-s-tools' },
    children: [{
      path: 'change',
      name: 'change',
      component: () => import('@/views/ChangePassword/index'),
      meta: { title: '修改密码', icon: '' }
    },]
  },
  {
    path: '*',
    redirect: '/404',
    hidden: true
  },
]
// 公共页面--但是渲染的时候必须放在最后
export const notfound = [
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]
const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
